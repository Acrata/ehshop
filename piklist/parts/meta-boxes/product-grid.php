<?php
/**
 * Title: Product grid
 * Post Type: page
 * Template: template-home
 */
piklist('field', array(
    'type' => 'group'
    ,'field' => 'grid_home'
    ,'label' => __('Address (Grouped)', 'piklist-demo')
    ,'template' => 'field'
    ,'description' => __('A grouped field with the field parameter set.', 'piklist-demo')
    ,'fields' => array(
      array(
        'type' => 'group'
        ,'add_more' => true
        ,'label' => __('Address (Grouped)', 'piklist-demo')
        ,'columns' => 2
        ,'template' => 'field'
        ,'description' => __('A grouped field with the field parameter set.', 'piklist-demo')
        ,'fields' => array(
          array(
            'type' => 'file'
            ,'field' => 'img-grid'
            ,'scope' => 'post_meta'
            ,'label' => 'Media Uploader'
            ,'options' => array(
            'modal_title' => 'Add File(s)'
            ,'button' => 'Add'
            ,'save' => 'url'
            )
          )
          ,array(
            'type' => 'text'
            ,'field' => 'caption_text'
            ,'label' => __('City', 'piklist-demo')
            ,'columns' => 5
            ,'attributes' => array(
              'placeholder' => 'City'
            )
          )
        )
      )

      
    )
  ));