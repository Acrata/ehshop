<?php
/**
 * Title: Hero Image (Cabecera)
 * Post Type: page
 * Template: template-home
 */

 piklist('field', array(
    'type' => 'file'
    ,'field' => 'hero_image'
    ,'scope' => 'post_meta'
    ,'label' => 'Hero image'
    ,'options' => array(
      'modal_title' => 'Add File(s)'
      ,'button' => 'Add'
      ,'save' => 'url'
    )
  ));