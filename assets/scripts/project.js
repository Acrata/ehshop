'use strict';

/**
 * Show/Hide the Search Form in the header.
 *
 * @author Corey Collins
 */
window.ShowHideSearchForm = {};
(function (window, $, app) {

	// Constructor
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things
	app.cache = function () {
		app.$c = {
			window: $(window),
			body: $('body'),
			headerSearchForm: $('.site-header-action .cta-button')
		};
	};

	// Combine all events
	app.bindEvents = function () {
		app.$c.headerSearchForm.on('keyup touchstart click', app.showHideSearchForm);
		app.$c.body.on('keyup touchstart click', app.hideSearchForm);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.headerSearchForm.length;
	};

	// Adds the toggle class for the search form.
	app.showHideSearchForm = function () {
		app.$c.body.toggleClass('search-form-visible');
	};

	// Hides the search form if we click outside of its container.
	app.hideSearchForm = function (event) {

		if (!$(event.target).parents('div').hasClass('site-header-action')) {
			app.$c.body.removeClass('search-form-visible');
		}
	};

	// Engage
	$(app.init);
})(window, jQuery, window.ShowHideSearchForm);
"use strict";

window.FixedMenu = {};
(function (window, $, app) {
   var headroom = new Headroom();
   $("#header").headroom({
      "offset": 0,
      "tolerance": 5,
      "classes": {
         "initial": "animated",
         "pinned": "slideInUp-up",
         "unpinned": "slideInDown-down"
      }
   });
   console.log($("#header"));

   var secondaryNav = $('.cd-secondary-nav'),
       secondaryNavTopPosition = secondaryNav.offset().top;

   $(window).on('scroll', function () {
      if ($(window).scrollTop() > secondaryNavTopPosition) {
         secondaryNav.addClass('is-fixed');
         setTimeout(function () {
            secondaryNav.addClass('animate-children');
            $('#cd-logo').addClass('slide-in');
            $('.cd-btn').addClass('slide-in');
         }, 50);
      } else {
         secondaryNav.removeClass('is-fixed');
         setTimeout(function () {
            secondaryNav.removeClass('animate-children');
            $('#cd-logo').removeClass('slide-in');
            $('.cd-btn').removeClass('slide-in');
         }, 50);
      }
   });
   console.log(headroom);
})(window, jQuery, window.FixedMenu);
'use strict';

/**
 * File hero-carousel.js
 *
 * Create a carousel if we have more than one hero slide.
 */
window.wdsHeroCarousel = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			heroCarousel: $('.carousel')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.doSlick);
		app.$c.window.on('load', app.doFirstAnimation);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.heroCarousel.length;
	};

	// Animate the first slide on window load.
	app.doFirstAnimation = function () {

		// Get the first slide content area and animation attribute.
		var firstSlide = app.$c.heroCarousel.find('[data-slick-index=0]'),
		    firstSlideContent = firstSlide.find('.hero-content'),
		    firstAnimation = firstSlideContent.attr('data-animation');

		// Add the animation class to the first slide.
		firstSlideContent.addClass(firstAnimation);
	};

	// Animate the slide content.
	app.doAnimation = function () {
		var slides = $('.slide'),
		    activeSlide = $('.slick-current'),
		    activeContent = activeSlide.find('.hero-content'),


		// This is a string like so: 'animated someCssClass'.
		animationClass = activeContent.attr('data-animation'),
		    splitAnimation = animationClass.split(' '),


		// This is the 'animated' class.
		animationTrigger = splitAnimation[0];

		// Go through each slide to see if we've already set animation classes.
		slides.each(function () {
			var slideContent = $(this).find('.hero-content');

			// If we've set animation classes on a slide, remove them.
			if (slideContent.hasClass('animated')) {

				// Get the last class, which is the animate.css class.
				var lastClass = slideContent.attr('class').split(' ').pop();

				// Remove both animation classes.
				slideContent.removeClass(lastClass).removeClass(animationTrigger);
			}
		});

		// Add animation classes after slide is in view.
		activeContent.addClass(animationClass);
	};

	// Allow background videos to autoplay.
	app.playBackgroundVideos = function () {

		// Get all the videos in our slides object.
		$('video').each(function () {

			// Let them autoplay. TODO: Possibly change this later to only play the visible slide video.
			this.play();
		});
	};

	// Kick off Slick.
	app.doSlick = function () {
		app.$c.heroCarousel.on('init', app.playBackgroundVideos);

		app.$c.heroCarousel.slick({
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: false,
			dots: false,
			focusOnSelect: true,
			waitForAnimate: true
		});

		app.$c.heroCarousel.on('afterChange', app.doAnimation);
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsHeroCarousel);
'use strict';

/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 */
document.body.className = document.body.className.replace('no-js', 'js');
"use strict";

window.wrMenuDropDown = {};
(function (window, $, app) {
    // Constructor.
    app.init = function () {
        var menuHeight = $(".menu").height();
        $(".stacked").find(".sub-menu").each(function () {
            $(this).addClass("foo");
        });
        $(".stacked-item").parent("ul").each(function () {
            console.log($(this));
            console.log(menuHeight);
            $(".stacked-item .foo li").removeAttr("style");
            $(".stacked-item .foo").css("cssText", "position: absolute !important");
            $(".stacked-item .foo").css("left", "0");
            $(".stacked-item .foo").css("paddingTop", menuHeight);
            // $(".stacked").css("height", menuHeight + stackedHeight)
        });
        $(".menu-item-has-children").hover(function () {
            console.log($(this).parent());
            var stackedHeight = $(".stacked").height();
            $(".stacked").css("height", menuHeight + stackedHeight);
        }, function () {
            var stackedHeight = $(".stacked").height();
            $(".stacked").css("height", stackedHeight);
        });
        console.log($(".top-stripe").height());
    };
    // Engage!
    $(app.init);
})(window, jQuery, window.wrMenuDropDown);
'use strict';

/**
 * File: mobile-menu.js
 *
 * Create an accordion style dropdown.
 */
window.wdsMobileMenu = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			window: $(window),
			subMenuContainer: $('.mobile-menu .sub-menu, .utility-navigation .sub-menu'),
			subSubMenuContainer: $('.mobile-menu .sub-menu .sub-menu'),
			subMenuParentItem: $('.mobile-menu li.menu-item-has-children, .utility-navigation li.menu-item-has-children'),
			offCanvasContainer: $('.off-canvas-container')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.window.on('load', app.appendWrMega);
		app.$c.subMenuParentItem.on('click', app.toggleSubmenu);
		app.$c.subMenuParentItem.on('transitionend', app.resetSubMenu);
		app.$c.offCanvasContainer.on('transitionend', app.forceCloseSubmenus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Reset the submenus after it's done closing.
	app.resetSubMenu = function () {

		// When the list item is done transitioning in height,
		// remove the classes from the submenu so it is ready to toggle again.
		if ($(this).is('li.menu-item-has-children') && !$(this).hasClass('is-visible')) {
			$(this).find('ul.sub-menu').removeClass('slideOutLeft is-visible');
		}
	};

	// Slide out the submenu items.
	app.slideOutSubMenus = function (el) {

		// If this item's parent is visible and this is not, bail.
		if (el.parent().hasClass('is-visible') && !el.hasClass('is-visible')) {
			return;
		}

		// If this item's parent is visible and this item is visible, hide its submenu then bail.
		if (el.parent().hasClass('is-visible') && el.hasClass('is-visible')) {
			el.removeClass('is-visible').find('.sub-menu').removeClass('slideInLeft').addClass('slideOutLeft');
			return;
		}

		app.$c.subMenuContainer.each(function () {

			// Only try to close submenus that are actually open.
			if ($(this).hasClass('slideInLeft')) {

				// Close the parent list item, and set the corresponding button aria to false.
				$(this).parent().removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);

				// Slide out the submenu.
				$(this).removeClass('slideInLeft').addClass('slideOutLeft');
			}
		});
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.prepend('<button type="button" aria-expanded="false" class="parent-indicator" aria-label="Open submenu"><span class="down-arrow"></span></button>');
	};

	// Add the down arrow to submenu parents.
	app.appendWrMega = function () {
		console.log("wr megamenu go here!");
	};
	app.appendWrMega;
	// Deal with the submenu.
	app.toggleSubmenu = function (e) {

		var el = $(this),
		    // The menu element which was clicked on.
		subMenu = el.children('ul.sub-menu'),
		    // The nearest submenu.
		$target = $(e.target); // the element that's actually being clicked (child of the li that triggered the click event).

		// Figure out if we're clicking the button or its arrow child,
		// if so, we can just open or close the menu and bail.
		if ($target.hasClass('down-arrow') || $target.hasClass('parent-indicator')) {

			// First, collapse any already opened submenus.
			app.slideOutSubMenus(el);

			if (!subMenu.hasClass('is-visible')) {

				// Open the submenu.
				app.openSubmenu(el, subMenu);
			}

			return false;
		}
	};

	// Open a submenu.
	app.openSubmenu = function (parent, subMenu) {

		// Expand the list menu item, and set the corresponding button aria to true.
		parent.addClass('is-visible').find('.parent-indicator').attr('aria-expanded', true);

		// Slide the menu in.
		subMenu.addClass('is-visible animated slideInLeft');
	};

	// Force close all the submenus when the main menu container is closed.
	app.forceCloseSubmenus = function () {

		// The transitionend event triggers on open and on close, need to make sure we only do this on close.
		if (!$(this).hasClass('is-visible')) {
			app.$c.subMenuParentItem.removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);
			app.$c.subMenuContainer.removeClass('is-visible slideInLeft');
			app.$c.body.css('overflow', 'visible');
			app.$c.body.unbind('touchstart');
		}

		if ($(this).hasClass('is-visible')) {
			app.$c.body.css('overflow', 'hidden');
			app.$c.body.bind('touchstart', function (e) {
				if (!$(e.target).parents('.contact-modal')[0]) {
					e.preventDefault();
				}
			});
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsMobileMenu);

jQuery(window).load(function () {
	var is_mobile_browser = function is_mobile_browser() {
		if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
			return true;
		} else {
			return false;
		}
	};
	if (is_mobile_browser) {
		//jQuery( ".off-canvas-container" ).append( jQuery(".centered-menu") );
		jQuery(".centered-menu").clone().appendTo(jQuery(".off-canvas-container"));
		//jQuery( ".centered-menu" ).before( jQuery(".home-logo") );
	}
	//TODO query conditional, look wrmegamenu for examples.
});
'use strict';

/**
 * File modal.js
 *
 * Deal with multiple modals and their media.
 */
window.wdsModal = {};
(function (window, $, app) {

	var $modalToggle = void 0,
	    $focusableChildren = void 0,
	    $player = void 0,
	    $tag = document.createElement('script'),
	    $firstScriptTag = document.getElementsByTagName('script')[0],
	    YT = void 0;

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			$firstScriptTag.parentNode.insertBefore($tag, $firstScriptTag);
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return $('.modal-trigger').length;
	};

	// Combine all events.
	app.bindEvents = function () {

		// Trigger a modal to open.
		app.$c.body.on('click touchstart', '.modal-trigger', app.openModal);

		// Trigger the close button to close the modal.
		app.$c.body.on('click touchstart', '.close', app.closeModal);

		// Allow the user to close the modal by hitting the esc key.
		app.$c.body.on('keydown', app.escKeyClose);

		// Allow the user to close the modal by clicking outside of the modal.
		app.$c.body.on('click touchstart', 'div.modal-open', app.closeModalByClick);

		// Listen to tabs, trap keyboard if we need to
		app.$c.body.on('keydown', app.trapKeyboardMaybe);
	};

	// Open the modal.
	app.openModal = function () {

		// Store the modal toggle element
		$modalToggle = $(this);

		// Figure out which modal we're opening and store the object.
		var $modal = $($(this).data('target'));

		// Display the modal.
		$modal.addClass('modal-open');

		// Add body class.
		app.$c.body.addClass('modal-open');

		// Find the focusable children of the modal.
		// This list may be incomplete, really wish jQuery had the :focusable pseudo like jQuery UI does.
		// For more about :input see: https://api.jquery.com/input-selector/
		$focusableChildren = $modal.find('a, :input, [tabindex]');

		// Ideally, there is always one (the close button), but you never know.
		if (0 < $focusableChildren.length) {

			// Shift focus to the first focusable element.
			$focusableChildren[0].focus();
		}
	};

	// Close the modal.
	app.closeModal = function () {

		// Figure the opened modal we're closing and store the object.
		var $modal = $($('div.modal-open .close').data('target')),


		// Find the iframe in the $modal object.
		$iframe = $modal.find('iframe');

		// Only do this if there are any iframes.
		if ($iframe.length) {

			// Get the iframe src URL.
			var url = $iframe.attr('src');

			// Removing/Readding the URL will effectively break the YouTube API.
			// So let's not do that when the iframe URL contains the enablejsapi parameter.
			if (!url.includes('enablejsapi=1')) {

				// Remove the source URL, then add it back, so the video can be played again later.
				$iframe.attr('src', '').attr('src', url);
			} else {

				// Use the YouTube API to stop the video.
				$player.stopVideo();
			}
		}

		// Finally, hide the modal.
		$modal.removeClass('modal-open');

		// Remove the body class.
		app.$c.body.removeClass('modal-open');

		// Revert focus back to toggle element
		$modalToggle.focus();
	};

	// Close if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeModal();
		}
	};

	// Close if the user clicks outside of the modal
	app.closeModalByClick = function (event) {

		// If the parent container is NOT the modal dialog container, close the modal
		if (!$(event.target).parents('div').hasClass('modal-dialog')) {
			app.closeModal();
		}
	};

	// Trap the keyboard into a modal when one is active.
	app.trapKeyboardMaybe = function (event) {

		// We only need to do stuff when the modal is open and tab is pressed.
		if (9 === event.which && 0 < $('.modal-open').length) {
			var $focused = $(':focus'),
			    focusIndex = $focusableChildren.index($focused);

			if (0 === focusIndex && event.shiftKey) {

				// If this is the first focusable element, and shift is held when pressing tab, go back to last focusable element.
				$focusableChildren[$focusableChildren.length - 1].focus();
				event.preventDefault();
			} else if (!event.shiftKey && focusIndex === $focusableChildren.length - 1) {

				// If this is the last focusable element, and shift is not held, go back to the first focusable element.
				$focusableChildren[0].focus();
				event.preventDefault();
			}
		}
	};

	// Hook into YouTube <iframe>.
	app.onYouTubeIframeAPIReady = function () {
		var $modal = $('div.modal'),
		    $iframeid = $modal.find('iframe').attr('id');

		$player = new YT.Player($iframeid, {
			events: {
				'onReady': app.onPlayerReady,
				'onStateChange': app.onPlayerStateChange
			}
		});
	};

	// Do something on player ready.
	app.onPlayerReady = function () {};

	// Do something on player state change.
	app.onPlayerStateChange = function () {

		// Set focus to the first focusable element inside of the modal the player is in.
		$(event.target.a).parents('.modal').find('a, :input, [tabindex]').first().focus();
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsModal);
'use strict';

/**
 * File: navigation-primary.js
 *
 * Helpers for the primary navigation.
 */
window.wdsPrimaryNavigation = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			subMenuContainer: $('.main-navigation .sub-menu'),
			subMenuParentItem: $('.main-navigation li.menu-item-has-children')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.find('a').on('focusin focusout', app.toggleFocus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.find('> a').append('<span class="caret-down" aria-hidden="true"></span>');
	};

	// Toggle the focus class on the link parent.
	app.toggleFocus = function () {
		$(this).parents('li.menu-item-has-children').toggleClass('focus');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsPrimaryNavigation);
'use strict';

/**
 * File: off-canvas.js
 *
 * Help deal with the off-canvas mobile menu.
 */
window.wdsoffCanvas = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			offCanvasClose: $('.off-canvas-close'),
			offCanvasContainer: $('.off-canvas-container'),
			offCanvasOpen: $('.off-canvas-open'),
			offCanvasScreen: $('.off-canvas-screen')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.body.on('keydown', app.escKeyClose);
		app.$c.offCanvasClose.on('click', app.closeoffCanvas);
		app.$c.offCanvasOpen.on('click', app.toggleoffCanvas);
		app.$c.offCanvasScreen.on('click', app.closeoffCanvas);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.offCanvasContainer.length;
	};

	// To show or not to show?
	app.toggleoffCanvas = function () {

		if ('true' === $(this).attr('aria-expanded')) {
			app.closeoffCanvas();
		} else {
			app.openoffCanvas();
		}
	};

	// Show that drawer!
	app.openoffCanvas = function () {
		app.$c.offCanvasContainer.addClass('is-visible');
		app.$c.offCanvasOpen.addClass('is-visible');
		app.$c.offCanvasScreen.addClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', true);
		app.$c.offCanvasContainer.attr('aria-hidden', false);

		app.$c.offCanvasContainer.find('button').first().focus();
	};

	// Close that drawer!
	app.closeoffCanvas = function () {
		app.$c.offCanvasContainer.removeClass('is-visible');
		app.$c.offCanvasOpen.removeClass('is-visible');
		app.$c.offCanvasScreen.removeClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', false);
		app.$c.offCanvasContainer.attr('aria-hidden', true);

		app.$c.offCanvasOpen.focus();
	};

	// Close drawer if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeoffCanvas();
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsoffCanvas);
'use strict';

window.EhShop = {};
(function (window, $, app) {

    // Constructor.
    app.init = function () {
        $('.woof_products_top_panel').css('display', 'none');

        //	app.slideInit('.slide-best');
    };
    app.slideInit = function (toSlide) {};
    // Engage!
    $(app.init);
})(window, jQuery, window.EhShop);
'use strict';

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
	var isWebkit = -1 < navigator.userAgent.toLowerCase().indexOf('webkit'),
	    isOpera = -1 < navigator.userAgent.toLowerCase().indexOf('opera'),
	    isIe = -1 < navigator.userAgent.toLowerCase().indexOf('msie');

	if ((isWebkit || isOpera || isIe) && document.getElementById && window.addEventListener) {
		window.addEventListener('hashchange', function () {
			var id = location.hash.substring(1),
			    element;

			if (!/^[A-z0-9_-]+$/.test(id)) {
				return;
			}

			element = document.getElementById(id);

			if (element) {
				if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false);
	}
})();
'use strict';

window.slickEhshop = {};
(function (window, $, app) {

    // Constructor.
    app.init = function () {
        app.slideInit('.slide-best');
    };
    app.slideInit = function (toSlide) {
        $(toSlide).slick({

            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            responsive: [{
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            }]
        });
    };
    /*     $(document).ready(function(){
            $('.slide-best').slick({
                
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });
        }); */
    // Engage!
    $(app.init);
})(window, jQuery, window.slickEhshop);
'use strict';

/**
 * File window-ready.js
 *
 * Add a "ready" class to <body> when window is ready.
 */
window.wdsWindowReady = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();
		app.bindEvents();
	};

	// Cache document elements.
	app.cache = function () {
		app.$c = {
			'window': $(window),
			'body': $(document.body)
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.load(app.addBodyClass);
	};

	// Add a class to <body>.
	app.addBodyClass = function () {
		app.$c.body.addClass('ready');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsWindowReady);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlYWRlci1idXR0b24uanMiLCJoZWFkcm9vbS5qcyIsImhlcm8tY2Fyb3VzZWwuanMiLCJqcy1lbmFibGVkLmpzIiwibWVudS13ci5qcyIsIm1vYmlsZS1tZW51LmpzIiwibW9kYWwuanMiLCJuYXZpZ2F0aW9uLXByaW1hcnkuanMiLCJvZmYtY2FudmFzLmpzIiwic2hvcC1laHNob3AuanMiLCJza2lwLWxpbmstZm9jdXMtZml4LmpzIiwic2xpY2stZWhzaG9wLmpzIiwid2luZG93LXJlYWR5LmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsIlNob3dIaWRlU2VhcmNoRm9ybSIsIiQiLCJhcHAiLCJpbml0IiwiY2FjaGUiLCJtZWV0c1JlcXVpcmVtZW50cyIsImJpbmRFdmVudHMiLCIkYyIsImJvZHkiLCJoZWFkZXJTZWFyY2hGb3JtIiwib24iLCJzaG93SGlkZVNlYXJjaEZvcm0iLCJoaWRlU2VhcmNoRm9ybSIsImxlbmd0aCIsInRvZ2dsZUNsYXNzIiwiZXZlbnQiLCJ0YXJnZXQiLCJwYXJlbnRzIiwiaGFzQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImpRdWVyeSIsIkZpeGVkTWVudSIsImhlYWRyb29tIiwiSGVhZHJvb20iLCJjb25zb2xlIiwibG9nIiwic2Vjb25kYXJ5TmF2Iiwic2Vjb25kYXJ5TmF2VG9wUG9zaXRpb24iLCJvZmZzZXQiLCJ0b3AiLCJzY3JvbGxUb3AiLCJhZGRDbGFzcyIsInNldFRpbWVvdXQiLCJ3ZHNIZXJvQ2Fyb3VzZWwiLCJoZXJvQ2Fyb3VzZWwiLCJkb1NsaWNrIiwiZG9GaXJzdEFuaW1hdGlvbiIsImZpcnN0U2xpZGUiLCJmaW5kIiwiZmlyc3RTbGlkZUNvbnRlbnQiLCJmaXJzdEFuaW1hdGlvbiIsImF0dHIiLCJkb0FuaW1hdGlvbiIsInNsaWRlcyIsImFjdGl2ZVNsaWRlIiwiYWN0aXZlQ29udGVudCIsImFuaW1hdGlvbkNsYXNzIiwic3BsaXRBbmltYXRpb24iLCJzcGxpdCIsImFuaW1hdGlvblRyaWdnZXIiLCJlYWNoIiwic2xpZGVDb250ZW50IiwibGFzdENsYXNzIiwicG9wIiwicGxheUJhY2tncm91bmRWaWRlb3MiLCJwbGF5Iiwic2xpY2siLCJhdXRvcGxheSIsImF1dG9wbGF5U3BlZWQiLCJhcnJvd3MiLCJkb3RzIiwiZm9jdXNPblNlbGVjdCIsIndhaXRGb3JBbmltYXRlIiwiZG9jdW1lbnQiLCJjbGFzc05hbWUiLCJyZXBsYWNlIiwid3JNZW51RHJvcERvd24iLCJtZW51SGVpZ2h0IiwiaGVpZ2h0IiwicGFyZW50IiwicmVtb3ZlQXR0ciIsImNzcyIsImhvdmVyIiwic3RhY2tlZEhlaWdodCIsIndkc01vYmlsZU1lbnUiLCJzdWJNZW51Q29udGFpbmVyIiwic3ViU3ViTWVudUNvbnRhaW5lciIsInN1Yk1lbnVQYXJlbnRJdGVtIiwib2ZmQ2FudmFzQ29udGFpbmVyIiwiYWRkRG93bkFycm93IiwiYXBwZW5kV3JNZWdhIiwidG9nZ2xlU3VibWVudSIsInJlc2V0U3ViTWVudSIsImZvcmNlQ2xvc2VTdWJtZW51cyIsImlzIiwic2xpZGVPdXRTdWJNZW51cyIsImVsIiwicHJlcGVuZCIsImUiLCJzdWJNZW51IiwiY2hpbGRyZW4iLCIkdGFyZ2V0Iiwib3BlblN1Ym1lbnUiLCJ1bmJpbmQiLCJiaW5kIiwicHJldmVudERlZmF1bHQiLCJsb2FkIiwiaXNfbW9iaWxlX2Jyb3dzZXIiLCJuYXZpZ2F0b3IiLCJ1c2VyQWdlbnQiLCJtYXRjaCIsImNsb25lIiwiYXBwZW5kVG8iLCJ3ZHNNb2RhbCIsIiRtb2RhbFRvZ2dsZSIsIiRmb2N1c2FibGVDaGlsZHJlbiIsIiRwbGF5ZXIiLCIkdGFnIiwiY3JlYXRlRWxlbWVudCIsIiRmaXJzdFNjcmlwdFRhZyIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwiWVQiLCJwYXJlbnROb2RlIiwiaW5zZXJ0QmVmb3JlIiwib3Blbk1vZGFsIiwiY2xvc2VNb2RhbCIsImVzY0tleUNsb3NlIiwiY2xvc2VNb2RhbEJ5Q2xpY2siLCJ0cmFwS2V5Ym9hcmRNYXliZSIsIiRtb2RhbCIsImRhdGEiLCJmb2N1cyIsIiRpZnJhbWUiLCJ1cmwiLCJpbmNsdWRlcyIsInN0b3BWaWRlbyIsImtleUNvZGUiLCJ3aGljaCIsIiRmb2N1c2VkIiwiZm9jdXNJbmRleCIsImluZGV4Iiwic2hpZnRLZXkiLCJvbllvdVR1YmVJZnJhbWVBUElSZWFkeSIsIiRpZnJhbWVpZCIsIlBsYXllciIsImV2ZW50cyIsIm9uUGxheWVyUmVhZHkiLCJvblBsYXllclN0YXRlQ2hhbmdlIiwiYSIsImZpcnN0Iiwid2RzUHJpbWFyeU5hdmlnYXRpb24iLCJ0b2dnbGVGb2N1cyIsImFwcGVuZCIsIndkc29mZkNhbnZhcyIsIm9mZkNhbnZhc0Nsb3NlIiwib2ZmQ2FudmFzT3BlbiIsIm9mZkNhbnZhc1NjcmVlbiIsImNsb3Nlb2ZmQ2FudmFzIiwidG9nZ2xlb2ZmQ2FudmFzIiwib3Blbm9mZkNhbnZhcyIsIkVoU2hvcCIsInNsaWRlSW5pdCIsInRvU2xpZGUiLCJpc1dlYmtpdCIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiIsImlzT3BlcmEiLCJpc0llIiwiZ2V0RWxlbWVudEJ5SWQiLCJhZGRFdmVudExpc3RlbmVyIiwiaWQiLCJsb2NhdGlvbiIsImhhc2giLCJzdWJzdHJpbmciLCJlbGVtZW50IiwidGVzdCIsInRhZ05hbWUiLCJ0YWJJbmRleCIsInNsaWNrRWhzaG9wIiwic2xpZGVzVG9TaG93Iiwic2xpZGVzVG9TY3JvbGwiLCJyZXNwb25zaXZlIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwiaW5maW5pdGUiLCJ3ZHNXaW5kb3dSZWFkeSIsImFkZEJvZHlDbGFzcyJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7QUFLQUEsT0FBT0Msa0JBQVAsR0FBNEIsRUFBNUI7QUFDRSxXQUFVRCxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMkI7O0FBRTVCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFXO0FBQ3JCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUtGLElBQUlHLGlCQUFKLEVBQUwsRUFBK0I7QUFDOUJILE9BQUlJLFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQUosS0FBSUUsS0FBSixHQUFZLFlBQVc7QUFDdEJGLE1BQUlLLEVBQUosR0FBUztBQUNSUixXQUFRRSxFQUFHRixNQUFILENBREE7QUFFUlMsU0FBTVAsRUFBRyxNQUFILENBRkU7QUFHUlEscUJBQWtCUixFQUFHLGlDQUFIO0FBSFYsR0FBVDtBQUtBLEVBTkQ7O0FBUUE7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9FLGdCQUFQLENBQXdCQyxFQUF4QixDQUE0Qix3QkFBNUIsRUFBc0RSLElBQUlTLGtCQUExRDtBQUNBVCxNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWUUsRUFBWixDQUFnQix3QkFBaEIsRUFBMENSLElBQUlVLGNBQTlDO0FBQ0EsRUFIRDs7QUFLQTtBQUNBVixLQUFJRyxpQkFBSixHQUF3QixZQUFXO0FBQ2xDLFNBQU9ILElBQUlLLEVBQUosQ0FBT0UsZ0JBQVAsQ0FBd0JJLE1BQS9CO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWCxLQUFJUyxrQkFBSixHQUF5QixZQUFXO0FBQ25DVCxNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWU0sV0FBWixDQUF5QixxQkFBekI7QUFDQSxFQUZEOztBQUlBO0FBQ0FaLEtBQUlVLGNBQUosR0FBcUIsVUFBVUcsS0FBVixFQUFrQjs7QUFFdEMsTUFBSyxDQUFFZCxFQUFHYyxNQUFNQyxNQUFULEVBQWtCQyxPQUFsQixDQUEyQixLQUEzQixFQUFtQ0MsUUFBbkMsQ0FBNkMsb0JBQTdDLENBQVAsRUFBNkU7QUFDNUVoQixPQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWVcsV0FBWixDQUF5QixxQkFBekI7QUFDQTtBQUNELEVBTEQ7O0FBT0E7QUFDQWxCLEdBQUdDLElBQUlDLElBQVA7QUFFQSxDQS9DQyxFQStDRUosTUEvQ0YsRUErQ1VxQixNQS9DVixFQStDa0JyQixPQUFPQyxrQkEvQ3pCLENBQUY7OztBQ05BRCxPQUFPc0IsU0FBUCxHQUFtQixFQUFuQjtBQUNFLFdBQVV0QixNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMEI7QUFDNUIsT0FBSW9CLFdBQVcsSUFBSUMsUUFBSixFQUFmO0FBQ0F0QixLQUFFLFNBQUYsRUFBYXFCLFFBQWIsQ0FBc0I7QUFDcEIsZ0JBQVUsQ0FEVTtBQUVwQixtQkFBYSxDQUZPO0FBR3BCLGlCQUFXO0FBQ1Qsb0JBQVcsVUFERjtBQUVULG1CQUFVLGNBRkQ7QUFHVCxxQkFBWTtBQUhIO0FBSFMsSUFBdEI7QUFTQUUsV0FBUUMsR0FBUixDQUFZeEIsRUFBRSxTQUFGLENBQVo7O0FBRUEsT0FBSXlCLGVBQWV6QixFQUFFLG1CQUFGLENBQW5CO0FBQUEsT0FDSTBCLDBCQUEwQkQsYUFBYUUsTUFBYixHQUFzQkMsR0FEcEQ7O0FBR0E1QixLQUFFRixNQUFGLEVBQVVXLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFlBQVU7QUFDOUIsVUFBR1QsRUFBRUYsTUFBRixFQUFVK0IsU0FBVixLQUF3QkgsdUJBQTNCLEVBQXFEO0FBQ2xERCxzQkFBYUssUUFBYixDQUFzQixVQUF0QjtBQUNBQyxvQkFBVyxZQUFXO0FBQ25CTix5QkFBYUssUUFBYixDQUFzQixrQkFBdEI7QUFDQTlCLGNBQUUsVUFBRixFQUFjOEIsUUFBZCxDQUF1QixVQUF2QjtBQUNBOUIsY0FBRSxTQUFGLEVBQWE4QixRQUFiLENBQXNCLFVBQXRCO0FBQ0YsVUFKRCxFQUlHLEVBSkg7QUFLRixPQVBELE1BT087QUFDSkwsc0JBQWFQLFdBQWIsQ0FBeUIsVUFBekI7QUFDQWEsb0JBQVcsWUFBVztBQUNuQk4seUJBQWFQLFdBQWIsQ0FBeUIsa0JBQXpCO0FBQ0FsQixjQUFFLFVBQUYsRUFBY2tCLFdBQWQsQ0FBMEIsVUFBMUI7QUFDQWxCLGNBQUUsU0FBRixFQUFha0IsV0FBYixDQUF5QixVQUF6QjtBQUNGLFVBSkQsRUFJRyxFQUpIO0FBS0Y7QUFDSCxJQWhCRDtBQWlCQUssV0FBUUMsR0FBUixDQUFZSCxRQUFaO0FBQ0MsQ0FsQ0MsRUFrQ0V2QixNQWxDRixFQWtDVXFCLE1BbENWLEVBa0NrQnJCLE9BQU9zQixTQWxDekIsQ0FBRjs7O0FDREE7Ozs7O0FBS0F0QixPQUFPa0MsZUFBUCxHQUF5QixFQUF6QjtBQUNFLFdBQVVsQyxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMkI7O0FBRTVCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFXO0FBQ3JCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUtGLElBQUlHLGlCQUFKLEVBQUwsRUFBK0I7QUFDOUJILE9BQUlJLFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQUosS0FBSUUsS0FBSixHQUFZLFlBQVc7QUFDdEJGLE1BQUlLLEVBQUosR0FBUztBQUNSUixXQUFRRSxFQUFHRixNQUFILENBREE7QUFFUm1DLGlCQUFjakMsRUFBRyxXQUFIO0FBRk4sR0FBVDtBQUlBLEVBTEQ7O0FBT0E7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1csRUFBZCxDQUFrQixNQUFsQixFQUEwQlIsSUFBSWlDLE9BQTlCO0FBQ0FqQyxNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1csRUFBZCxDQUFrQixNQUFsQixFQUEwQlIsSUFBSWtDLGdCQUE5QjtBQUNBLEVBSEQ7O0FBS0E7QUFDQWxDLEtBQUlHLGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT0gsSUFBSUssRUFBSixDQUFPMkIsWUFBUCxDQUFvQnJCLE1BQTNCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWCxLQUFJa0MsZ0JBQUosR0FBdUIsWUFBVzs7QUFFakM7QUFDQSxNQUFJQyxhQUFhbkMsSUFBSUssRUFBSixDQUFPMkIsWUFBUCxDQUFvQkksSUFBcEIsQ0FBMEIsc0JBQTFCLENBQWpCO0FBQUEsTUFDQ0Msb0JBQW9CRixXQUFXQyxJQUFYLENBQWlCLGVBQWpCLENBRHJCO0FBQUEsTUFFQ0UsaUJBQWlCRCxrQkFBa0JFLElBQWxCLENBQXdCLGdCQUF4QixDQUZsQjs7QUFJQTtBQUNBRixvQkFBa0JSLFFBQWxCLENBQTRCUyxjQUE1QjtBQUNBLEVBVEQ7O0FBV0E7QUFDQXRDLEtBQUl3QyxXQUFKLEdBQWtCLFlBQVc7QUFDNUIsTUFBSUMsU0FBUzFDLEVBQUcsUUFBSCxDQUFiO0FBQUEsTUFDQzJDLGNBQWMzQyxFQUFHLGdCQUFILENBRGY7QUFBQSxNQUVDNEMsZ0JBQWdCRCxZQUFZTixJQUFaLENBQWtCLGVBQWxCLENBRmpCOzs7QUFJQztBQUNBUSxtQkFBaUJELGNBQWNKLElBQWQsQ0FBb0IsZ0JBQXBCLENBTGxCO0FBQUEsTUFNQ00saUJBQWlCRCxlQUFlRSxLQUFmLENBQXNCLEdBQXRCLENBTmxCOzs7QUFRQztBQUNBQyxxQkFBbUJGLGVBQWUsQ0FBZixDQVRwQjs7QUFXQTtBQUNBSixTQUFPTyxJQUFQLENBQWEsWUFBVztBQUN2QixPQUFJQyxlQUFlbEQsRUFBRyxJQUFILEVBQVVxQyxJQUFWLENBQWdCLGVBQWhCLENBQW5COztBQUVBO0FBQ0EsT0FBS2EsYUFBYWpDLFFBQWIsQ0FBdUIsVUFBdkIsQ0FBTCxFQUEyQzs7QUFFMUM7QUFDQSxRQUFJa0MsWUFBWUQsYUFDZFYsSUFEYyxDQUNSLE9BRFEsRUFFZE8sS0FGYyxDQUVQLEdBRk8sRUFHZEssR0FIYyxFQUFoQjs7QUFLQTtBQUNBRixpQkFBYWhDLFdBQWIsQ0FBMEJpQyxTQUExQixFQUFzQ2pDLFdBQXRDLENBQW1EOEIsZ0JBQW5EO0FBQ0E7QUFDRCxHQWZEOztBQWlCQTtBQUNBSixnQkFBY2QsUUFBZCxDQUF3QmUsY0FBeEI7QUFDQSxFQWhDRDs7QUFrQ0E7QUFDQTVDLEtBQUlvRCxvQkFBSixHQUEyQixZQUFXOztBQUVyQztBQUNBckQsSUFBRyxPQUFILEVBQWFpRCxJQUFiLENBQW1CLFlBQVc7O0FBRTdCO0FBQ0EsUUFBS0ssSUFBTDtBQUNBLEdBSkQ7QUFLQSxFQVJEOztBQVVBO0FBQ0FyRCxLQUFJaUMsT0FBSixHQUFjLFlBQVc7QUFDeEJqQyxNQUFJSyxFQUFKLENBQU8yQixZQUFQLENBQW9CeEIsRUFBcEIsQ0FBd0IsTUFBeEIsRUFBZ0NSLElBQUlvRCxvQkFBcEM7O0FBRUFwRCxNQUFJSyxFQUFKLENBQU8yQixZQUFQLENBQW9Cc0IsS0FBcEIsQ0FBMkI7QUFDMUJDLGFBQVUsSUFEZ0I7QUFFMUJDLGtCQUFlLElBRlc7QUFHMUJDLFdBQVEsS0FIa0I7QUFJMUJDLFNBQU0sS0FKb0I7QUFLMUJDLGtCQUFlLElBTFc7QUFNMUJDLG1CQUFnQjtBQU5VLEdBQTNCOztBQVNBNUQsTUFBSUssRUFBSixDQUFPMkIsWUFBUCxDQUFvQnhCLEVBQXBCLENBQXdCLGFBQXhCLEVBQXVDUixJQUFJd0MsV0FBM0M7QUFDQSxFQWJEOztBQWVBO0FBQ0F6QyxHQUFHQyxJQUFJQyxJQUFQO0FBQ0EsQ0ExR0MsRUEwR0VKLE1BMUdGLEVBMEdVcUIsTUExR1YsRUEwR2tCckIsT0FBT2tDLGVBMUd6QixDQUFGOzs7QUNOQTs7Ozs7QUFLQThCLFNBQVN2RCxJQUFULENBQWN3RCxTQUFkLEdBQTBCRCxTQUFTdkQsSUFBVCxDQUFjd0QsU0FBZCxDQUF3QkMsT0FBeEIsQ0FBaUMsT0FBakMsRUFBMEMsSUFBMUMsQ0FBMUI7OztBQ0xBbEUsT0FBT21FLGNBQVAsR0FBd0IsRUFBeEI7QUFDRSxXQUFVbkUsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTBCO0FBQ3pCO0FBQ0ZBLFFBQUlDLElBQUosR0FBVyxZQUFXO0FBQ25CLFlBQUlnRSxhQUFhbEUsRUFBRSxPQUFGLEVBQVdtRSxNQUFYLEVBQWpCO0FBQ0FuRSxVQUFFLFVBQUYsRUFBY3FDLElBQWQsQ0FBbUIsV0FBbkIsRUFBZ0NZLElBQWhDLENBQXFDLFlBQVU7QUFDL0NqRCxjQUFFLElBQUYsRUFBUThCLFFBQVIsQ0FBaUIsS0FBakI7QUFDSCxTQUZHO0FBR0E5QixVQUFFLGVBQUYsRUFBbUJvRSxNQUFuQixDQUEwQixJQUExQixFQUFnQ25CLElBQWhDLENBQXFDLFlBQVc7QUFDNUMxQixvQkFBUUMsR0FBUixDQUFZeEIsRUFBRSxJQUFGLENBQVo7QUFDQXVCLG9CQUFRQyxHQUFSLENBQVkwQyxVQUFaO0FBQ0FsRSxjQUFFLHVCQUFGLEVBQTJCcUUsVUFBM0IsQ0FBc0MsT0FBdEM7QUFDQXJFLGNBQUUsb0JBQUYsRUFBd0JzRSxHQUF4QixDQUE0QixTQUE1QixFQUFzQywrQkFBdEM7QUFDQXRFLGNBQUUsb0JBQUYsRUFBd0JzRSxHQUF4QixDQUE0QixNQUE1QixFQUFtQyxHQUFuQztBQUNBdEUsY0FBRSxvQkFBRixFQUF3QnNFLEdBQXhCLENBQTRCLFlBQTVCLEVBQXlDSixVQUF6QztBQUNEO0FBQ0YsU0FSRDtBQVNBbEUsVUFBRyx5QkFBSCxFQUErQnVFLEtBQS9CLENBQ0YsWUFBVztBQUNUaEQsb0JBQVFDLEdBQVIsQ0FBWXhCLEVBQUUsSUFBRixFQUFRb0UsTUFBUixFQUFaO0FBQ0EsZ0JBQUlJLGdCQUFnQnhFLEVBQUUsVUFBRixFQUFjbUUsTUFBZCxFQUFwQjtBQUNJbkUsY0FBRSxVQUFGLEVBQWNzRSxHQUFkLENBQWtCLFFBQWxCLEVBQTRCSixhQUFhTSxhQUF6QztBQUNMLFNBTEMsRUFLQyxZQUFXO0FBQ1osZ0JBQUlBLGdCQUFnQnhFLEVBQUUsVUFBRixFQUFjbUUsTUFBZCxFQUFwQjtBQUNJbkUsY0FBRSxVQUFGLEVBQWNzRSxHQUFkLENBQWtCLFFBQWxCLEVBQTRCRSxhQUE1QjtBQUNMLFNBUkM7QUFVRWpELGdCQUFRQyxHQUFSLENBQVl4QixFQUFFLGFBQUYsRUFBaUJtRSxNQUFqQixFQUFaO0FBQ0QsS0F6Qko7QUEwQkc7QUFDQW5FLE1BQUdDLElBQUlDLElBQVA7QUFDSCxDQTlCQyxFQThCRUosTUE5QkYsRUE4QlVxQixNQTlCVixFQThCa0JyQixPQUFPbUUsY0E5QnpCLENBQUY7OztBQ0RBOzs7OztBQUtBbkUsT0FBTzJFLGFBQVAsR0FBdUIsRUFBdkI7QUFDRSxXQUFVM0UsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QjtBQUNBQSxLQUFJQyxJQUFKLEdBQVcsWUFBVztBQUNyQkQsTUFBSUUsS0FBSjs7QUFFQSxNQUFLRixJQUFJRyxpQkFBSixFQUFMLEVBQStCO0FBQzlCSCxPQUFJSSxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFXO0FBQ3RCRixNQUFJSyxFQUFKLEdBQVM7QUFDUkMsU0FBTVAsRUFBRyxNQUFILENBREU7QUFFUkYsV0FBUUUsRUFBR0YsTUFBSCxDQUZBO0FBR1I0RSxxQkFBa0IxRSxFQUFHLHVEQUFILENBSFY7QUFJUjJFLHdCQUFxQjNFLEVBQUcsa0NBQUgsQ0FKYjtBQUtSNEUsc0JBQW1CNUUsRUFBRyx1RkFBSCxDQUxYO0FBTVI2RSx1QkFBb0I3RSxFQUFHLHVCQUFIO0FBTlosR0FBVDtBQVFBLEVBVEQ7O0FBV0E7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1csRUFBZCxDQUFrQixNQUFsQixFQUEwQlIsSUFBSTZFLFlBQTlCO0FBQ0E3RSxNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1csRUFBZCxDQUFrQixNQUFsQixFQUEwQlIsSUFBSThFLFlBQTlCO0FBQ0E5RSxNQUFJSyxFQUFKLENBQU9zRSxpQkFBUCxDQUF5Qm5FLEVBQXpCLENBQTZCLE9BQTdCLEVBQXNDUixJQUFJK0UsYUFBMUM7QUFDQS9FLE1BQUlLLEVBQUosQ0FBT3NFLGlCQUFQLENBQXlCbkUsRUFBekIsQ0FBNkIsZUFBN0IsRUFBOENSLElBQUlnRixZQUFsRDtBQUNBaEYsTUFBSUssRUFBSixDQUFPdUUsa0JBQVAsQ0FBMEJwRSxFQUExQixDQUE4QixlQUE5QixFQUErQ1IsSUFBSWlGLGtCQUFuRDtBQUNBLEVBTkQ7O0FBUUE7QUFDQWpGLEtBQUlHLGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT0gsSUFBSUssRUFBSixDQUFPb0UsZ0JBQVAsQ0FBd0I5RCxNQUEvQjtBQUNBLEVBRkQ7O0FBSUE7QUFDQVgsS0FBSWdGLFlBQUosR0FBbUIsWUFBVzs7QUFFN0I7QUFDQTtBQUNBLE1BQUtqRixFQUFHLElBQUgsRUFBVW1GLEVBQVYsQ0FBYywyQkFBZCxLQUErQyxDQUFFbkYsRUFBRyxJQUFILEVBQVVpQixRQUFWLENBQW9CLFlBQXBCLENBQXRELEVBQTJGO0FBQzFGakIsS0FBRyxJQUFILEVBQVVxQyxJQUFWLENBQWdCLGFBQWhCLEVBQWdDbkIsV0FBaEMsQ0FBNkMseUJBQTdDO0FBQ0E7QUFFRCxFQVJEOztBQVVBO0FBQ0FqQixLQUFJbUYsZ0JBQUosR0FBdUIsVUFBVUMsRUFBVixFQUFlOztBQUVyQztBQUNBLE1BQUtBLEdBQUdqQixNQUFILEdBQVluRCxRQUFaLENBQXNCLFlBQXRCLEtBQXdDLENBQUVvRSxHQUFHcEUsUUFBSCxDQUFhLFlBQWIsQ0FBL0MsRUFBNkU7QUFDNUU7QUFDQTs7QUFFRDtBQUNBLE1BQUtvRSxHQUFHakIsTUFBSCxHQUFZbkQsUUFBWixDQUFzQixZQUF0QixLQUF3Q29FLEdBQUdwRSxRQUFILENBQWEsWUFBYixDQUE3QyxFQUEyRTtBQUMxRW9FLE1BQUduRSxXQUFILENBQWdCLFlBQWhCLEVBQStCbUIsSUFBL0IsQ0FBcUMsV0FBckMsRUFBbURuQixXQUFuRCxDQUFnRSxhQUFoRSxFQUFnRlksUUFBaEYsQ0FBMEYsY0FBMUY7QUFDQTtBQUNBOztBQUVEN0IsTUFBSUssRUFBSixDQUFPb0UsZ0JBQVAsQ0FBd0J6QixJQUF4QixDQUE4QixZQUFXOztBQUV4QztBQUNBLE9BQUtqRCxFQUFHLElBQUgsRUFBVWlCLFFBQVYsQ0FBb0IsYUFBcEIsQ0FBTCxFQUEyQzs7QUFFMUM7QUFDQWpCLE1BQUcsSUFBSCxFQUFVb0UsTUFBVixHQUFtQmxELFdBQW5CLENBQWdDLFlBQWhDLEVBQStDbUIsSUFBL0MsQ0FBcUQsbUJBQXJELEVBQTJFRyxJQUEzRSxDQUFpRixlQUFqRixFQUFrRyxLQUFsRzs7QUFFQTtBQUNBeEMsTUFBRyxJQUFILEVBQVVrQixXQUFWLENBQXVCLGFBQXZCLEVBQXVDWSxRQUF2QyxDQUFpRCxjQUFqRDtBQUNBO0FBRUQsR0FaRDtBQWFBLEVBMUJEOztBQTRCQTtBQUNBN0IsS0FBSTZFLFlBQUosR0FBbUIsWUFBVztBQUM3QjdFLE1BQUlLLEVBQUosQ0FBT3NFLGlCQUFQLENBQXlCVSxPQUF6QixDQUFrQywwSUFBbEM7QUFDQSxFQUZEOztBQUlBO0FBQ0FyRixLQUFJOEUsWUFBSixHQUFtQixZQUFXO0FBQzdCeEQsVUFBUUMsR0FBUixDQUFZLHNCQUFaO0FBQ0EsRUFGRDtBQUdBdkIsS0FBSThFLFlBQUo7QUFDQTtBQUNBOUUsS0FBSStFLGFBQUosR0FBb0IsVUFBVU8sQ0FBVixFQUFjOztBQUVqQyxNQUFJRixLQUFLckYsRUFBRyxJQUFILENBQVQ7QUFBQSxNQUFvQjtBQUNuQndGLFlBQVVILEdBQUdJLFFBQUgsQ0FBYSxhQUFiLENBRFg7QUFBQSxNQUN5QztBQUN4Q0MsWUFBVTFGLEVBQUd1RixFQUFFeEUsTUFBTCxDQUZYLENBRmlDLENBSVA7O0FBRTFCO0FBQ0E7QUFDQSxNQUFLMkUsUUFBUXpFLFFBQVIsQ0FBa0IsWUFBbEIsS0FBb0N5RSxRQUFRekUsUUFBUixDQUFrQixrQkFBbEIsQ0FBekMsRUFBa0Y7O0FBRWpGO0FBQ0FoQixPQUFJbUYsZ0JBQUosQ0FBc0JDLEVBQXRCOztBQUVBLE9BQUssQ0FBRUcsUUFBUXZFLFFBQVIsQ0FBa0IsWUFBbEIsQ0FBUCxFQUEwQzs7QUFFekM7QUFDQWhCLFFBQUkwRixXQUFKLENBQWlCTixFQUFqQixFQUFxQkcsT0FBckI7QUFFQTs7QUFFRCxVQUFPLEtBQVA7QUFDQTtBQUVELEVBdkJEOztBQXlCQTtBQUNBdkYsS0FBSTBGLFdBQUosR0FBa0IsVUFBVXZCLE1BQVYsRUFBa0JvQixPQUFsQixFQUE0Qjs7QUFFN0M7QUFDQXBCLFNBQU90QyxRQUFQLENBQWlCLFlBQWpCLEVBQWdDTyxJQUFoQyxDQUFzQyxtQkFBdEMsRUFBNERHLElBQTVELENBQWtFLGVBQWxFLEVBQW1GLElBQW5GOztBQUVBO0FBQ0FnRCxVQUFRMUQsUUFBUixDQUFrQixpQ0FBbEI7QUFDQSxFQVBEOztBQVNBO0FBQ0E3QixLQUFJaUYsa0JBQUosR0FBeUIsWUFBVzs7QUFFbkM7QUFDQSxNQUFLLENBQUVsRixFQUFHLElBQUgsRUFBVWlCLFFBQVYsQ0FBb0IsWUFBcEIsQ0FBUCxFQUE0QztBQUMzQ2hCLE9BQUlLLEVBQUosQ0FBT3NFLGlCQUFQLENBQXlCMUQsV0FBekIsQ0FBc0MsWUFBdEMsRUFBcURtQixJQUFyRCxDQUEyRCxtQkFBM0QsRUFBaUZHLElBQWpGLENBQXVGLGVBQXZGLEVBQXdHLEtBQXhHO0FBQ0F2QyxPQUFJSyxFQUFKLENBQU9vRSxnQkFBUCxDQUF3QnhELFdBQXhCLENBQXFDLHdCQUFyQztBQUNBakIsT0FBSUssRUFBSixDQUFPQyxJQUFQLENBQVkrRCxHQUFaLENBQWlCLFVBQWpCLEVBQTZCLFNBQTdCO0FBQ0FyRSxPQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWXFGLE1BQVosQ0FBb0IsWUFBcEI7QUFDQTs7QUFFRCxNQUFLNUYsRUFBRyxJQUFILEVBQVVpQixRQUFWLENBQW9CLFlBQXBCLENBQUwsRUFBMEM7QUFDekNoQixPQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWStELEdBQVosQ0FBaUIsVUFBakIsRUFBNkIsUUFBN0I7QUFDQXJFLE9BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZc0YsSUFBWixDQUFrQixZQUFsQixFQUFnQyxVQUFVTixDQUFWLEVBQWM7QUFDN0MsUUFBSyxDQUFFdkYsRUFBR3VGLEVBQUV4RSxNQUFMLEVBQWNDLE9BQWQsQ0FBdUIsZ0JBQXZCLEVBQTBDLENBQTFDLENBQVAsRUFBc0Q7QUFDckR1RSxPQUFFTyxjQUFGO0FBQ0E7QUFDRCxJQUpEO0FBS0E7QUFDRCxFQWxCRDs7QUFvQkE7QUFDQTlGLEdBQUdDLElBQUlDLElBQVA7QUFFQSxDQW5KQyxFQW1KQ0osTUFuSkQsRUFtSlNxQixNQW5KVCxFQW1KaUJyQixPQUFPMkUsYUFuSnhCLENBQUY7O0FBc0pFdEQsT0FBT3JCLE1BQVAsRUFBZWlHLElBQWYsQ0FBb0IsWUFBVztBQUM5QixLQUFJQyxvQkFBb0IsU0FBcEJBLGlCQUFvQixHQUFZO0FBQy9CLE1BQUlDLFVBQVVDLFNBQVYsQ0FBb0JDLEtBQXBCLENBQTBCLFVBQTFCLEtBQ09GLFVBQVVDLFNBQVYsQ0FBb0JDLEtBQXBCLENBQTBCLFFBQTFCLENBRFAsSUFFT0YsVUFBVUMsU0FBVixDQUFvQkMsS0FBcEIsQ0FBMEIsU0FBMUIsQ0FGUCxJQUdPRixVQUFVQyxTQUFWLENBQW9CQyxLQUFwQixDQUEwQixPQUExQixDQUhQLElBSU9GLFVBQVVDLFNBQVYsQ0FBb0JDLEtBQXBCLENBQTBCLE9BQTFCLENBSlAsSUFLT0YsVUFBVUMsU0FBVixDQUFvQkMsS0FBcEIsQ0FBMEIsYUFBMUIsQ0FMUCxJQU1PRixVQUFVQyxTQUFWLENBQW9CQyxLQUFwQixDQUEwQixnQkFBMUIsQ0FOWCxFQU9FO0FBQ0UsVUFBTyxJQUFQO0FBQ0gsR0FURCxNQVNPO0FBQ0gsVUFBTyxLQUFQO0FBQ0g7QUFDSixFQWJGO0FBY0EsS0FBSUgsaUJBQUosRUFBdUI7QUFDeEI7QUFDQTdFLFNBQVEsZ0JBQVIsRUFBMkJpRixLQUEzQixHQUFtQ0MsUUFBbkMsQ0FBNkNsRixPQUFPLHVCQUFQLENBQTdDO0FBQ0E7QUFFRTtBQUNGO0FBQ0QsQ0F0QkM7OztBQzVKRjs7Ozs7QUFLQXJCLE9BQU93RyxRQUFQLEdBQWtCLEVBQWxCO0FBQ0UsV0FBVXhHLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUIsS0FBSXNHLHFCQUFKO0FBQUEsS0FDQ0MsMkJBREQ7QUFBQSxLQUVDQyxnQkFGRDtBQUFBLEtBR0NDLE9BQU81QyxTQUFTNkMsYUFBVCxDQUF3QixRQUF4QixDQUhSO0FBQUEsS0FJQ0Msa0JBQWtCOUMsU0FBUytDLG9CQUFULENBQStCLFFBQS9CLEVBQTBDLENBQTFDLENBSm5CO0FBQUEsS0FLQ0MsV0FMRDs7QUFPQTtBQUNBN0csS0FBSUMsSUFBSixHQUFXLFlBQVc7QUFDckJELE1BQUlFLEtBQUo7O0FBRUEsTUFBS0YsSUFBSUcsaUJBQUosRUFBTCxFQUErQjtBQUM5QndHLG1CQUFnQkcsVUFBaEIsQ0FBMkJDLFlBQTNCLENBQXlDTixJQUF6QyxFQUErQ0UsZUFBL0M7QUFDQTNHLE9BQUlJLFVBQUo7QUFDQTtBQUNELEVBUEQ7O0FBU0E7QUFDQUosS0FBSUUsS0FBSixHQUFZLFlBQVc7QUFDdEJGLE1BQUlLLEVBQUosR0FBUztBQUNSLFdBQVFOLEVBQUcsTUFBSDtBQURBLEdBQVQ7QUFHQSxFQUpEOztBQU1BO0FBQ0FDLEtBQUlHLGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT0osRUFBRyxnQkFBSCxFQUFzQlksTUFBN0I7QUFDQSxFQUZEOztBQUlBO0FBQ0FYLEtBQUlJLFVBQUosR0FBaUIsWUFBVzs7QUFFM0I7QUFDQUosTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlFLEVBQVosQ0FBZ0Isa0JBQWhCLEVBQW9DLGdCQUFwQyxFQUFzRFIsSUFBSWdILFNBQTFEOztBQUVBO0FBQ0FoSCxNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWUUsRUFBWixDQUFnQixrQkFBaEIsRUFBb0MsUUFBcEMsRUFBOENSLElBQUlpSCxVQUFsRDs7QUFFQTtBQUNBakgsTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlFLEVBQVosQ0FBZ0IsU0FBaEIsRUFBMkJSLElBQUlrSCxXQUEvQjs7QUFFQTtBQUNBbEgsTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlFLEVBQVosQ0FBZ0Isa0JBQWhCLEVBQW9DLGdCQUFwQyxFQUFzRFIsSUFBSW1ILGlCQUExRDs7QUFFQTtBQUNBbkgsTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlFLEVBQVosQ0FBZ0IsU0FBaEIsRUFBMkJSLElBQUlvSCxpQkFBL0I7QUFFQSxFQWpCRDs7QUFtQkE7QUFDQXBILEtBQUlnSCxTQUFKLEdBQWdCLFlBQVc7O0FBRTFCO0FBQ0FWLGlCQUFldkcsRUFBRyxJQUFILENBQWY7O0FBRUE7QUFDQSxNQUFJc0gsU0FBU3RILEVBQUdBLEVBQUcsSUFBSCxFQUFVdUgsSUFBVixDQUFnQixRQUFoQixDQUFILENBQWI7O0FBRUE7QUFDQUQsU0FBT3hGLFFBQVAsQ0FBaUIsWUFBakI7O0FBRUE7QUFDQTdCLE1BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZdUIsUUFBWixDQUFzQixZQUF0Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTBFLHVCQUFxQmMsT0FBT2pGLElBQVAsQ0FBYSx1QkFBYixDQUFyQjs7QUFFQTtBQUNBLE1BQUssSUFBSW1FLG1CQUFtQjVGLE1BQTVCLEVBQXFDOztBQUVwQztBQUNBNEYsc0JBQW1CLENBQW5CLEVBQXNCZ0IsS0FBdEI7QUFDQTtBQUVELEVBMUJEOztBQTRCQTtBQUNBdkgsS0FBSWlILFVBQUosR0FBaUIsWUFBVzs7QUFFM0I7QUFDQSxNQUFJSSxTQUFTdEgsRUFBR0EsRUFBRyx1QkFBSCxFQUE2QnVILElBQTdCLENBQW1DLFFBQW5DLENBQUgsQ0FBYjs7O0FBRUM7QUFDQUUsWUFBVUgsT0FBT2pGLElBQVAsQ0FBYSxRQUFiLENBSFg7O0FBS0E7QUFDQSxNQUFLb0YsUUFBUTdHLE1BQWIsRUFBc0I7O0FBRXJCO0FBQ0EsT0FBSThHLE1BQU1ELFFBQVFqRixJQUFSLENBQWMsS0FBZCxDQUFWOztBQUVBO0FBQ0E7QUFDQSxPQUFLLENBQUVrRixJQUFJQyxRQUFKLENBQWMsZUFBZCxDQUFQLEVBQXlDOztBQUV4QztBQUNBRixZQUFRakYsSUFBUixDQUFjLEtBQWQsRUFBcUIsRUFBckIsRUFBMEJBLElBQTFCLENBQWdDLEtBQWhDLEVBQXVDa0YsR0FBdkM7QUFDQSxJQUpELE1BSU87O0FBRU47QUFDQWpCLFlBQVFtQixTQUFSO0FBQ0E7QUFDRDs7QUFFRDtBQUNBTixTQUFPcEcsV0FBUCxDQUFvQixZQUFwQjs7QUFFQTtBQUNBakIsTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlXLFdBQVosQ0FBeUIsWUFBekI7O0FBRUE7QUFDQXFGLGVBQWFpQixLQUFiO0FBRUEsRUFwQ0Q7O0FBc0NBO0FBQ0F2SCxLQUFJa0gsV0FBSixHQUFrQixVQUFVckcsS0FBVixFQUFrQjtBQUNuQyxNQUFLLE9BQU9BLE1BQU0rRyxPQUFsQixFQUE0QjtBQUMzQjVILE9BQUlpSCxVQUFKO0FBQ0E7QUFDRCxFQUpEOztBQU1BO0FBQ0FqSCxLQUFJbUgsaUJBQUosR0FBd0IsVUFBVXRHLEtBQVYsRUFBa0I7O0FBRXpDO0FBQ0EsTUFBSyxDQUFFZCxFQUFHYyxNQUFNQyxNQUFULEVBQWtCQyxPQUFsQixDQUEyQixLQUEzQixFQUFtQ0MsUUFBbkMsQ0FBNkMsY0FBN0MsQ0FBUCxFQUF1RTtBQUN0RWhCLE9BQUlpSCxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0FqSCxLQUFJb0gsaUJBQUosR0FBd0IsVUFBVXZHLEtBQVYsRUFBa0I7O0FBRXpDO0FBQ0EsTUFBSyxNQUFNQSxNQUFNZ0gsS0FBWixJQUFxQixJQUFJOUgsRUFBRyxhQUFILEVBQW1CWSxNQUFqRCxFQUEwRDtBQUN6RCxPQUFJbUgsV0FBVy9ILEVBQUcsUUFBSCxDQUFmO0FBQUEsT0FDQ2dJLGFBQWF4QixtQkFBbUJ5QixLQUFuQixDQUEwQkYsUUFBMUIsQ0FEZDs7QUFHQSxPQUFLLE1BQU1DLFVBQU4sSUFBb0JsSCxNQUFNb0gsUUFBL0IsRUFBMEM7O0FBRXpDO0FBQ0ExQix1QkFBb0JBLG1CQUFtQjVGLE1BQW5CLEdBQTRCLENBQWhELEVBQW9ENEcsS0FBcEQ7QUFDQTFHLFVBQU1nRixjQUFOO0FBQ0EsSUFMRCxNQUtPLElBQUssQ0FBRWhGLE1BQU1vSCxRQUFSLElBQW9CRixlQUFleEIsbUJBQW1CNUYsTUFBbkIsR0FBNEIsQ0FBcEUsRUFBd0U7O0FBRTlFO0FBQ0E0Rix1QkFBbUIsQ0FBbkIsRUFBc0JnQixLQUF0QjtBQUNBMUcsVUFBTWdGLGNBQU47QUFDQTtBQUNEO0FBQ0QsRUFuQkQ7O0FBcUJBO0FBQ0E3RixLQUFJa0ksdUJBQUosR0FBOEIsWUFBVztBQUN4QyxNQUFJYixTQUFTdEgsRUFBRyxXQUFILENBQWI7QUFBQSxNQUNDb0ksWUFBWWQsT0FBT2pGLElBQVAsQ0FBYSxRQUFiLEVBQXdCRyxJQUF4QixDQUE4QixJQUE5QixDQURiOztBQUdBaUUsWUFBVSxJQUFJSyxHQUFHdUIsTUFBUCxDQUFlRCxTQUFmLEVBQTBCO0FBQ25DRSxXQUFRO0FBQ1AsZUFBV3JJLElBQUlzSSxhQURSO0FBRVAscUJBQWlCdEksSUFBSXVJO0FBRmQ7QUFEMkIsR0FBMUIsQ0FBVjtBQU1BLEVBVkQ7O0FBWUE7QUFDQXZJLEtBQUlzSSxhQUFKLEdBQW9CLFlBQVcsQ0FDOUIsQ0FERDs7QUFHQTtBQUNBdEksS0FBSXVJLG1CQUFKLEdBQTBCLFlBQVc7O0FBRXBDO0FBQ0F4SSxJQUFHYyxNQUFNQyxNQUFOLENBQWEwSCxDQUFoQixFQUFvQnpILE9BQXBCLENBQTZCLFFBQTdCLEVBQXdDcUIsSUFBeEMsQ0FBOEMsdUJBQTlDLEVBQXdFcUcsS0FBeEUsR0FBZ0ZsQixLQUFoRjtBQUNBLEVBSkQ7O0FBT0E7QUFDQXhILEdBQUdDLElBQUlDLElBQVA7QUFDQSxDQXhMQyxFQXdMQ0osTUF4TEQsRUF3TFNxQixNQXhMVCxFQXdMaUJyQixPQUFPd0csUUF4THhCLENBQUY7OztBQ05BOzs7OztBQUtBeEcsT0FBTzZJLG9CQUFQLEdBQThCLEVBQTlCO0FBQ0UsV0FBVTdJLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUI7QUFDQUEsS0FBSUMsSUFBSixHQUFXLFlBQVc7QUFDckJELE1BQUlFLEtBQUo7O0FBRUEsTUFBS0YsSUFBSUcsaUJBQUosRUFBTCxFQUErQjtBQUM5QkgsT0FBSUksVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBSixLQUFJRSxLQUFKLEdBQVksWUFBVztBQUN0QkYsTUFBSUssRUFBSixHQUFTO0FBQ1JSLFdBQVFFLEVBQUdGLE1BQUgsQ0FEQTtBQUVSNEUscUJBQWtCMUUsRUFBRyw0QkFBSCxDQUZWO0FBR1I0RSxzQkFBbUI1RSxFQUFHLDRDQUFIO0FBSFgsR0FBVDtBQUtBLEVBTkQ7O0FBUUE7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1csRUFBZCxDQUFrQixNQUFsQixFQUEwQlIsSUFBSTZFLFlBQTlCO0FBQ0E3RSxNQUFJSyxFQUFKLENBQU9zRSxpQkFBUCxDQUF5QnZDLElBQXpCLENBQStCLEdBQS9CLEVBQXFDNUIsRUFBckMsQ0FBeUMsa0JBQXpDLEVBQTZEUixJQUFJMkksV0FBakU7QUFDQSxFQUhEOztBQUtBO0FBQ0EzSSxLQUFJRyxpQkFBSixHQUF3QixZQUFXO0FBQ2xDLFNBQU9ILElBQUlLLEVBQUosQ0FBT29FLGdCQUFQLENBQXdCOUQsTUFBL0I7QUFDQSxFQUZEOztBQUlBO0FBQ0FYLEtBQUk2RSxZQUFKLEdBQW1CLFlBQVc7QUFDN0I3RSxNQUFJSyxFQUFKLENBQU9zRSxpQkFBUCxDQUF5QnZDLElBQXpCLENBQStCLEtBQS9CLEVBQXVDd0csTUFBdkMsQ0FBK0MscURBQS9DO0FBQ0EsRUFGRDs7QUFJQTtBQUNBNUksS0FBSTJJLFdBQUosR0FBa0IsWUFBVztBQUM1QjVJLElBQUcsSUFBSCxFQUFVZ0IsT0FBVixDQUFtQiwyQkFBbkIsRUFBaURILFdBQWpELENBQThELE9BQTlEO0FBQ0EsRUFGRDs7QUFJQTtBQUNBYixHQUFHQyxJQUFJQyxJQUFQO0FBRUEsQ0E1Q0MsRUE0Q0NKLE1BNUNELEVBNENTcUIsTUE1Q1QsRUE0Q2lCckIsT0FBTzZJLG9CQTVDeEIsQ0FBRjs7O0FDTkE7Ozs7O0FBS0E3SSxPQUFPZ0osWUFBUCxHQUFzQixFQUF0QjtBQUNFLFdBQVVoSixNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMkI7O0FBRTVCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFXO0FBQ3JCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUtGLElBQUlHLGlCQUFKLEVBQUwsRUFBK0I7QUFDOUJILE9BQUlJLFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQUosS0FBSUUsS0FBSixHQUFZLFlBQVc7QUFDdEJGLE1BQUlLLEVBQUosR0FBUztBQUNSQyxTQUFNUCxFQUFHLE1BQUgsQ0FERTtBQUVSK0ksbUJBQWdCL0ksRUFBRyxtQkFBSCxDQUZSO0FBR1I2RSx1QkFBb0I3RSxFQUFHLHVCQUFILENBSFo7QUFJUmdKLGtCQUFlaEosRUFBRyxrQkFBSCxDQUpQO0FBS1JpSixvQkFBaUJqSixFQUFHLG9CQUFIO0FBTFQsR0FBVDtBQU9BLEVBUkQ7O0FBVUE7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWUUsRUFBWixDQUFnQixTQUFoQixFQUEyQlIsSUFBSWtILFdBQS9CO0FBQ0FsSCxNQUFJSyxFQUFKLENBQU95SSxjQUFQLENBQXNCdEksRUFBdEIsQ0FBMEIsT0FBMUIsRUFBbUNSLElBQUlpSixjQUF2QztBQUNBakosTUFBSUssRUFBSixDQUFPMEksYUFBUCxDQUFxQnZJLEVBQXJCLENBQXlCLE9BQXpCLEVBQWtDUixJQUFJa0osZUFBdEM7QUFDQWxKLE1BQUlLLEVBQUosQ0FBTzJJLGVBQVAsQ0FBdUJ4SSxFQUF2QixDQUEyQixPQUEzQixFQUFvQ1IsSUFBSWlKLGNBQXhDO0FBQ0EsRUFMRDs7QUFPQTtBQUNBakosS0FBSUcsaUJBQUosR0FBd0IsWUFBVztBQUNsQyxTQUFPSCxJQUFJSyxFQUFKLENBQU91RSxrQkFBUCxDQUEwQmpFLE1BQWpDO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWCxLQUFJa0osZUFBSixHQUFzQixZQUFXOztBQUVoQyxNQUFLLFdBQVduSixFQUFHLElBQUgsRUFBVXdDLElBQVYsQ0FBZ0IsZUFBaEIsQ0FBaEIsRUFBb0Q7QUFDbkR2QyxPQUFJaUosY0FBSjtBQUNBLEdBRkQsTUFFTztBQUNOakosT0FBSW1KLGFBQUo7QUFDQTtBQUVELEVBUkQ7O0FBVUE7QUFDQW5KLEtBQUltSixhQUFKLEdBQW9CLFlBQVc7QUFDOUJuSixNQUFJSyxFQUFKLENBQU91RSxrQkFBUCxDQUEwQi9DLFFBQTFCLENBQW9DLFlBQXBDO0FBQ0E3QixNQUFJSyxFQUFKLENBQU8wSSxhQUFQLENBQXFCbEgsUUFBckIsQ0FBK0IsWUFBL0I7QUFDQTdCLE1BQUlLLEVBQUosQ0FBTzJJLGVBQVAsQ0FBdUJuSCxRQUF2QixDQUFpQyxZQUFqQzs7QUFFQTdCLE1BQUlLLEVBQUosQ0FBTzBJLGFBQVAsQ0FBcUJ4RyxJQUFyQixDQUEyQixlQUEzQixFQUE0QyxJQUE1QztBQUNBdkMsTUFBSUssRUFBSixDQUFPdUUsa0JBQVAsQ0FBMEJyQyxJQUExQixDQUFnQyxhQUFoQyxFQUErQyxLQUEvQzs7QUFFQXZDLE1BQUlLLEVBQUosQ0FBT3VFLGtCQUFQLENBQTBCeEMsSUFBMUIsQ0FBZ0MsUUFBaEMsRUFBMkNxRyxLQUEzQyxHQUFtRGxCLEtBQW5EO0FBQ0EsRUFURDs7QUFXQTtBQUNBdkgsS0FBSWlKLGNBQUosR0FBcUIsWUFBVztBQUMvQmpKLE1BQUlLLEVBQUosQ0FBT3VFLGtCQUFQLENBQTBCM0QsV0FBMUIsQ0FBdUMsWUFBdkM7QUFDQWpCLE1BQUlLLEVBQUosQ0FBTzBJLGFBQVAsQ0FBcUI5SCxXQUFyQixDQUFrQyxZQUFsQztBQUNBakIsTUFBSUssRUFBSixDQUFPMkksZUFBUCxDQUF1Qi9ILFdBQXZCLENBQW9DLFlBQXBDOztBQUVBakIsTUFBSUssRUFBSixDQUFPMEksYUFBUCxDQUFxQnhHLElBQXJCLENBQTJCLGVBQTNCLEVBQTRDLEtBQTVDO0FBQ0F2QyxNQUFJSyxFQUFKLENBQU91RSxrQkFBUCxDQUEwQnJDLElBQTFCLENBQWdDLGFBQWhDLEVBQStDLElBQS9DOztBQUVBdkMsTUFBSUssRUFBSixDQUFPMEksYUFBUCxDQUFxQnhCLEtBQXJCO0FBQ0EsRUFURDs7QUFXQTtBQUNBdkgsS0FBSWtILFdBQUosR0FBa0IsVUFBVXJHLEtBQVYsRUFBa0I7QUFDbkMsTUFBSyxPQUFPQSxNQUFNK0csT0FBbEIsRUFBNEI7QUFDM0I1SCxPQUFJaUosY0FBSjtBQUNBO0FBQ0QsRUFKRDs7QUFNQTtBQUNBbEosR0FBR0MsSUFBSUMsSUFBUDtBQUVBLENBaEZDLEVBZ0ZDSixNQWhGRCxFQWdGU3FCLE1BaEZULEVBZ0ZpQnJCLE9BQU9nSixZQWhGeEIsQ0FBRjs7O0FDTkFoSixPQUFPdUosTUFBUCxHQUFnQixFQUFoQjtBQUNFLFdBQVV2SixNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMEI7O0FBRzNCO0FBQ0FBLFFBQUlDLElBQUosR0FBVyxZQUFXO0FBQ2ZGLFVBQUUsMEJBQUYsRUFBOEJzRSxHQUE5QixDQUFrQyxTQUFsQyxFQUE0QyxNQUE1Qzs7QUFFUDtBQUVJLEtBTEo7QUFNR3JFLFFBQUlxSixTQUFKLEdBQWdCLFVBQVNDLE9BQVQsRUFBa0IsQ0FFakMsQ0FGRDtBQUdBO0FBQ0F2SixNQUFHQyxJQUFJQyxJQUFQO0FBQ0gsQ0FmQyxFQWVFSixNQWZGLEVBZVVxQixNQWZWLEVBZWtCckIsT0FBT3VKLE1BZnpCLENBQUY7OztBQ0RBOzs7Ozs7O0FBT0UsYUFBVztBQUNaLEtBQUlHLFdBQVcsQ0FBQyxDQUFELEdBQUt2RCxVQUFVQyxTQUFWLENBQW9CdUQsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLFFBQTNDLENBQXBCO0FBQUEsS0FDQ0MsVUFBVSxDQUFDLENBQUQsR0FBSzFELFVBQVVDLFNBQVYsQ0FBb0J1RCxXQUFwQixHQUFrQ0MsT0FBbEMsQ0FBMkMsT0FBM0MsQ0FEaEI7QUFBQSxLQUVDRSxPQUFPLENBQUMsQ0FBRCxHQUFLM0QsVUFBVUMsU0FBVixDQUFvQnVELFdBQXBCLEdBQWtDQyxPQUFsQyxDQUEyQyxNQUEzQyxDQUZiOztBQUlBLEtBQUssQ0FBRUYsWUFBWUcsT0FBWixJQUF1QkMsSUFBekIsS0FBbUM5RixTQUFTK0YsY0FBNUMsSUFBOEQvSixPQUFPZ0ssZ0JBQTFFLEVBQTZGO0FBQzVGaEssU0FBT2dLLGdCQUFQLENBQXlCLFlBQXpCLEVBQXVDLFlBQVc7QUFDakQsT0FBSUMsS0FBS0MsU0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXlCLENBQXpCLENBQVQ7QUFBQSxPQUNDQyxPQUREOztBQUdBLE9BQUssQ0FBSSxlQUFGLENBQW9CQyxJQUFwQixDQUEwQkwsRUFBMUIsQ0FBUCxFQUF3QztBQUN2QztBQUNBOztBQUVESSxhQUFVckcsU0FBUytGLGNBQVQsQ0FBeUJFLEVBQXpCLENBQVY7O0FBRUEsT0FBS0ksT0FBTCxFQUFlO0FBQ2QsUUFBSyxDQUFJLHVDQUFGLENBQTRDQyxJQUE1QyxDQUFrREQsUUFBUUUsT0FBMUQsQ0FBUCxFQUE2RTtBQUM1RUYsYUFBUUcsUUFBUixHQUFtQixDQUFDLENBQXBCO0FBQ0E7O0FBRURILFlBQVEzQyxLQUFSO0FBQ0E7QUFDRCxHQWpCRCxFQWlCRyxLQWpCSDtBQWtCQTtBQUNELENBekJDLEdBQUY7OztBQ1BBMUgsT0FBT3lLLFdBQVAsR0FBcUIsRUFBckI7QUFDRSxXQUFVekssTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTBCOztBQUczQjtBQUNBQSxRQUFJQyxJQUFKLEdBQVcsWUFBVztBQUNyQkQsWUFBSXFKLFNBQUosQ0FBYyxhQUFkO0FBRUcsS0FISjtBQUlHckosUUFBSXFKLFNBQUosR0FBZ0IsVUFBU0MsT0FBVCxFQUFrQjtBQUM5QnZKLFVBQUV1SixPQUFGLEVBQVdoRyxLQUFYLENBQWlCOztBQUViaUgsMEJBQWMsQ0FGRDtBQUdiQyw0QkFBZ0IsQ0FISDtBQUliakgsc0JBQVUsS0FKRztBQUtiQywyQkFBZSxJQUxGO0FBTWJpSCx3QkFBWSxDQUNSO0FBQ0lDLDRCQUFZLEdBRGhCO0FBRUlDLDBCQUFVO0FBQ05KLGtDQUFjLENBRFI7QUFFTkMsb0NBQWdCLENBRlY7QUFHTkksOEJBQVUsSUFISjtBQUlObEgsMEJBQU07QUFKQTtBQU9kO0FBQ0E7QUFDQTtBQVhBLGFBRFE7QUFOQyxTQUFqQjtBQXNCSCxLQXZCRDtBQXdCSjs7Ozs7Ozs7O0FBU0k7QUFDQTNELE1BQUdDLElBQUlDLElBQVA7QUFDSCxDQTNDQyxFQTJDRUosTUEzQ0YsRUEyQ1VxQixNQTNDVixFQTJDa0JyQixPQUFPeUssV0EzQ3pCLENBQUY7OztBQ0RBOzs7OztBQUtBekssT0FBT2dMLGNBQVAsR0FBd0IsRUFBeEI7QUFDRSxXQUFVaEwsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QjtBQUNBQSxLQUFJQyxJQUFKLEdBQVcsWUFBVztBQUNyQkQsTUFBSUUsS0FBSjtBQUNBRixNQUFJSSxVQUFKO0FBQ0EsRUFIRDs7QUFLQTtBQUNBSixLQUFJRSxLQUFKLEdBQVksWUFBVztBQUN0QkYsTUFBSUssRUFBSixHQUFTO0FBQ1IsYUFBVU4sRUFBR0YsTUFBSCxDQURGO0FBRVIsV0FBUUUsRUFBRzhELFNBQVN2RCxJQUFaO0FBRkEsR0FBVDtBQUlBLEVBTEQ7O0FBT0E7QUFDQU4sS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY2lHLElBQWQsQ0FBb0I5RixJQUFJOEssWUFBeEI7QUFDQSxFQUZEOztBQUlBO0FBQ0E5SyxLQUFJOEssWUFBSixHQUFtQixZQUFXO0FBQzdCOUssTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVl1QixRQUFaLENBQXNCLE9BQXRCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBOUIsR0FBR0MsSUFBSUMsSUFBUDtBQUNBLENBNUJDLEVBNEJDSixNQTVCRCxFQTRCU3FCLE1BNUJULEVBNEJpQnJCLE9BQU9nTCxjQTVCeEIsQ0FBRiIsImZpbGUiOiJwcm9qZWN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBTaG93L0hpZGUgdGhlIFNlYXJjaCBGb3JtIGluIHRoZSBoZWFkZXIuXG4gKlxuICogQGF1dGhvciBDb3JleSBDb2xsaW5zXG4gKi9cbndpbmRvdy5TaG93SGlkZVNlYXJjaEZvcm0gPSB7fTtcbiggZnVuY3Rpb24oIHdpbmRvdywgJCwgYXBwICkge1xuXG5cdC8vIENvbnN0cnVjdG9yXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLmNhY2hlKCk7XG5cblx0XHRpZiAoIGFwcC5tZWV0c1JlcXVpcmVtZW50cygpICkge1xuXHRcdFx0YXBwLmJpbmRFdmVudHMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gQ2FjaGUgYWxsIHRoZSB0aGluZ3Ncblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdGJvZHk6ICQoICdib2R5JyApLFxuXHRcdFx0aGVhZGVyU2VhcmNoRm9ybTogJCggJy5zaXRlLWhlYWRlci1hY3Rpb24gLmN0YS1idXR0b24nIClcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50c1xuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5oZWFkZXJTZWFyY2hGb3JtLm9uKCAna2V5dXAgdG91Y2hzdGFydCBjbGljaycsIGFwcC5zaG93SGlkZVNlYXJjaEZvcm0gKTtcblx0XHRhcHAuJGMuYm9keS5vbiggJ2tleXVwIHRvdWNoc3RhcnQgY2xpY2snLCBhcHAuaGlkZVNlYXJjaEZvcm0gKTtcblx0fTtcblxuXHQvLyBEbyB3ZSBtZWV0IHRoZSByZXF1aXJlbWVudHM/XG5cdGFwcC5tZWV0c1JlcXVpcmVtZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiBhcHAuJGMuaGVhZGVyU2VhcmNoRm9ybS5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQWRkcyB0aGUgdG9nZ2xlIGNsYXNzIGZvciB0aGUgc2VhcmNoIGZvcm0uXG5cdGFwcC5zaG93SGlkZVNlYXJjaEZvcm0gPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMuYm9keS50b2dnbGVDbGFzcyggJ3NlYXJjaC1mb3JtLXZpc2libGUnICk7XG5cdH07XG5cblx0Ly8gSGlkZXMgdGhlIHNlYXJjaCBmb3JtIGlmIHdlIGNsaWNrIG91dHNpZGUgb2YgaXRzIGNvbnRhaW5lci5cblx0YXBwLmhpZGVTZWFyY2hGb3JtID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXG5cdFx0aWYgKCAhICQoIGV2ZW50LnRhcmdldCApLnBhcmVudHMoICdkaXYnICkuaGFzQ2xhc3MoICdzaXRlLWhlYWRlci1hY3Rpb24nICkgKSB7XG5cdFx0XHRhcHAuJGMuYm9keS5yZW1vdmVDbGFzcyggJ3NlYXJjaC1mb3JtLXZpc2libGUnICk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIEVuZ2FnZVxuXHQkKCBhcHAuaW5pdCApO1xuXG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy5TaG93SGlkZVNlYXJjaEZvcm0gKSApO1xuIiwid2luZG93LkZpeGVkTWVudSA9IHt9O1xuKCBmdW5jdGlvbiggd2luZG93LCAkLCBhcHApIHtcbnZhciBoZWFkcm9vbSA9IG5ldyBIZWFkcm9vbSgpO1xuJChcIiNoZWFkZXJcIikuaGVhZHJvb20oe1xuICBcIm9mZnNldFwiOiAwLFxuICBcInRvbGVyYW5jZVwiOiA1LFxuICBcImNsYXNzZXNcIjoge1xuICAgIFwiaW5pdGlhbFwiOiBcImFuaW1hdGVkXCIsXG4gICAgXCJwaW5uZWRcIjogXCJzbGlkZUluVXAtdXBcIixcbiAgICBcInVucGlubmVkXCI6IFwic2xpZGVJbkRvd24tZG93blwiXG4gIH1cbn0pO1xuY29uc29sZS5sb2coJChcIiNoZWFkZXJcIikpO1xuXG52YXIgc2Vjb25kYXJ5TmF2ID0gJCgnLmNkLXNlY29uZGFyeS1uYXYnKSxcbiAgICBzZWNvbmRhcnlOYXZUb3BQb3NpdGlvbiA9IHNlY29uZGFyeU5hdi5vZmZzZXQoKS50b3A7XG5cbiQod2luZG93KS5vbignc2Nyb2xsJywgZnVuY3Rpb24oKXtcbiAgIGlmKCQod2luZG93KS5zY3JvbGxUb3AoKSA+IHNlY29uZGFyeU5hdlRvcFBvc2l0aW9uICkge1xuICAgICAgc2Vjb25kYXJ5TmF2LmFkZENsYXNzKCdpcy1maXhlZCcpO1xuICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgIHNlY29uZGFyeU5hdi5hZGRDbGFzcygnYW5pbWF0ZS1jaGlsZHJlbicpO1xuICAgICAgICAgJCgnI2NkLWxvZ28nKS5hZGRDbGFzcygnc2xpZGUtaW4nKTtcbiAgICAgICAgICQoJy5jZC1idG4nKS5hZGRDbGFzcygnc2xpZGUtaW4nKTtcbiAgICAgIH0sIDUwKTtcbiAgIH0gZWxzZSB7XG4gICAgICBzZWNvbmRhcnlOYXYucmVtb3ZlQ2xhc3MoJ2lzLWZpeGVkJyk7XG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgc2Vjb25kYXJ5TmF2LnJlbW92ZUNsYXNzKCdhbmltYXRlLWNoaWxkcmVuJyk7XG4gICAgICAgICAkKCcjY2QtbG9nbycpLnJlbW92ZUNsYXNzKCdzbGlkZS1pbicpO1xuICAgICAgICAgJCgnLmNkLWJ0bicpLnJlbW92ZUNsYXNzKCdzbGlkZS1pbicpO1xuICAgICAgfSwgNTApO1xuICAgfVxufSk7XG5jb25zb2xlLmxvZyhoZWFkcm9vbSk7XG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy5GaXhlZE1lbnUgKSk7XG4iLCIvKipcbiAqIEZpbGUgaGVyby1jYXJvdXNlbC5qc1xuICpcbiAqIENyZWF0ZSBhIGNhcm91c2VsIGlmIHdlIGhhdmUgbW9yZSB0aGFuIG9uZSBoZXJvIHNsaWRlLlxuICovXG53aW5kb3cud2RzSGVyb0Nhcm91c2VsID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdGhlcm9DYXJvdXNlbDogJCggJy5jYXJvdXNlbCcgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy53aW5kb3cub24oICdsb2FkJywgYXBwLmRvU2xpY2sgKTtcblx0XHRhcHAuJGMud2luZG93Lm9uKCAnbG9hZCcsIGFwcC5kb0ZpcnN0QW5pbWF0aW9uICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLmhlcm9DYXJvdXNlbC5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgZmlyc3Qgc2xpZGUgb24gd2luZG93IGxvYWQuXG5cdGFwcC5kb0ZpcnN0QW5pbWF0aW9uID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBHZXQgdGhlIGZpcnN0IHNsaWRlIGNvbnRlbnQgYXJlYSBhbmQgYW5pbWF0aW9uIGF0dHJpYnV0ZS5cblx0XHRsZXQgZmlyc3RTbGlkZSA9IGFwcC4kYy5oZXJvQ2Fyb3VzZWwuZmluZCggJ1tkYXRhLXNsaWNrLWluZGV4PTBdJyApLFxuXHRcdFx0Zmlyc3RTbGlkZUNvbnRlbnQgPSBmaXJzdFNsaWRlLmZpbmQoICcuaGVyby1jb250ZW50JyApLFxuXHRcdFx0Zmlyc3RBbmltYXRpb24gPSBmaXJzdFNsaWRlQ29udGVudC5hdHRyKCAnZGF0YS1hbmltYXRpb24nICk7XG5cblx0XHQvLyBBZGQgdGhlIGFuaW1hdGlvbiBjbGFzcyB0byB0aGUgZmlyc3Qgc2xpZGUuXG5cdFx0Zmlyc3RTbGlkZUNvbnRlbnQuYWRkQ2xhc3MoIGZpcnN0QW5pbWF0aW9uICk7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgc2xpZGUgY29udGVudC5cblx0YXBwLmRvQW5pbWF0aW9uID0gZnVuY3Rpb24oKSB7XG5cdFx0bGV0IHNsaWRlcyA9ICQoICcuc2xpZGUnICksXG5cdFx0XHRhY3RpdmVTbGlkZSA9ICQoICcuc2xpY2stY3VycmVudCcgKSxcblx0XHRcdGFjdGl2ZUNvbnRlbnQgPSBhY3RpdmVTbGlkZS5maW5kKCAnLmhlcm8tY29udGVudCcgKSxcblxuXHRcdFx0Ly8gVGhpcyBpcyBhIHN0cmluZyBsaWtlIHNvOiAnYW5pbWF0ZWQgc29tZUNzc0NsYXNzJy5cblx0XHRcdGFuaW1hdGlvbkNsYXNzID0gYWN0aXZlQ29udGVudC5hdHRyKCAnZGF0YS1hbmltYXRpb24nICksXG5cdFx0XHRzcGxpdEFuaW1hdGlvbiA9IGFuaW1hdGlvbkNsYXNzLnNwbGl0KCAnICcgKSxcblxuXHRcdFx0Ly8gVGhpcyBpcyB0aGUgJ2FuaW1hdGVkJyBjbGFzcy5cblx0XHRcdGFuaW1hdGlvblRyaWdnZXIgPSBzcGxpdEFuaW1hdGlvblswXTtcblxuXHRcdC8vIEdvIHRocm91Z2ggZWFjaCBzbGlkZSB0byBzZWUgaWYgd2UndmUgYWxyZWFkeSBzZXQgYW5pbWF0aW9uIGNsYXNzZXMuXG5cdFx0c2xpZGVzLmVhY2goIGZ1bmN0aW9uKCkge1xuXHRcdFx0bGV0IHNsaWRlQ29udGVudCA9ICQoIHRoaXMgKS5maW5kKCAnLmhlcm8tY29udGVudCcgKTtcblxuXHRcdFx0Ly8gSWYgd2UndmUgc2V0IGFuaW1hdGlvbiBjbGFzc2VzIG9uIGEgc2xpZGUsIHJlbW92ZSB0aGVtLlxuXHRcdFx0aWYgKCBzbGlkZUNvbnRlbnQuaGFzQ2xhc3MoICdhbmltYXRlZCcgKSApIHtcblxuXHRcdFx0XHQvLyBHZXQgdGhlIGxhc3QgY2xhc3MsIHdoaWNoIGlzIHRoZSBhbmltYXRlLmNzcyBjbGFzcy5cblx0XHRcdFx0bGV0IGxhc3RDbGFzcyA9IHNsaWRlQ29udGVudFxuXHRcdFx0XHRcdC5hdHRyKCAnY2xhc3MnIClcblx0XHRcdFx0XHQuc3BsaXQoICcgJyApXG5cdFx0XHRcdFx0LnBvcCgpO1xuXG5cdFx0XHRcdC8vIFJlbW92ZSBib3RoIGFuaW1hdGlvbiBjbGFzc2VzLlxuXHRcdFx0XHRzbGlkZUNvbnRlbnQucmVtb3ZlQ2xhc3MoIGxhc3RDbGFzcyApLnJlbW92ZUNsYXNzKCBhbmltYXRpb25UcmlnZ2VyICk7XG5cdFx0XHR9XG5cdFx0fSApO1xuXG5cdFx0Ly8gQWRkIGFuaW1hdGlvbiBjbGFzc2VzIGFmdGVyIHNsaWRlIGlzIGluIHZpZXcuXG5cdFx0YWN0aXZlQ29udGVudC5hZGRDbGFzcyggYW5pbWF0aW9uQ2xhc3MgKTtcblx0fTtcblxuXHQvLyBBbGxvdyBiYWNrZ3JvdW5kIHZpZGVvcyB0byBhdXRvcGxheS5cblx0YXBwLnBsYXlCYWNrZ3JvdW5kVmlkZW9zID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBHZXQgYWxsIHRoZSB2aWRlb3MgaW4gb3VyIHNsaWRlcyBvYmplY3QuXG5cdFx0JCggJ3ZpZGVvJyApLmVhY2goIGZ1bmN0aW9uKCkge1xuXG5cdFx0XHQvLyBMZXQgdGhlbSBhdXRvcGxheS4gVE9ETzogUG9zc2libHkgY2hhbmdlIHRoaXMgbGF0ZXIgdG8gb25seSBwbGF5IHRoZSB2aXNpYmxlIHNsaWRlIHZpZGVvLlxuXHRcdFx0dGhpcy5wbGF5KCk7XG5cdFx0fSApO1xuXHR9O1xuXG5cdC8vIEtpY2sgb2ZmIFNsaWNrLlxuXHRhcHAuZG9TbGljayA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5oZXJvQ2Fyb3VzZWwub24oICdpbml0JywgYXBwLnBsYXlCYWNrZ3JvdW5kVmlkZW9zICk7XG5cblx0XHRhcHAuJGMuaGVyb0Nhcm91c2VsLnNsaWNrKCB7XG5cdFx0XHRhdXRvcGxheTogdHJ1ZSxcblx0XHRcdGF1dG9wbGF5U3BlZWQ6IDUwMDAsXG5cdFx0XHRhcnJvd3M6IGZhbHNlLFxuXHRcdFx0ZG90czogZmFsc2UsXG5cdFx0XHRmb2N1c09uU2VsZWN0OiB0cnVlLFxuXHRcdFx0d2FpdEZvckFuaW1hdGU6IHRydWVcblx0XHR9ICk7XG5cblx0XHRhcHAuJGMuaGVyb0Nhcm91c2VsLm9uKCAnYWZ0ZXJDaGFuZ2UnLCBhcHAuZG9BbmltYXRpb24gKTtcblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNIZXJvQ2Fyb3VzZWwgKSApO1xuIiwiLyoqXG4gKiBGaWxlIGpzLWVuYWJsZWQuanNcbiAqXG4gKiBJZiBKYXZhc2NyaXB0IGlzIGVuYWJsZWQsIHJlcGxhY2UgdGhlIDxib2R5PiBjbGFzcyBcIm5vLWpzXCIuXG4gKi9cbmRvY3VtZW50LmJvZHkuY2xhc3NOYW1lID0gZG9jdW1lbnQuYm9keS5jbGFzc05hbWUucmVwbGFjZSggJ25vLWpzJywgJ2pzJyApO1xuIiwid2luZG93LndyTWVudURyb3BEb3duID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCkge1xuICAgLy8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24oKSB7XG4gICAgbGV0IG1lbnVIZWlnaHQgPSAkKFwiLm1lbnVcIikuaGVpZ2h0KCk7XG4gICAgJChcIi5zdGFja2VkXCIpLmZpbmQoXCIuc3ViLW1lbnVcIikuZWFjaChmdW5jdGlvbigpe1xuICAgICQodGhpcykuYWRkQ2xhc3MoXCJmb29cIik7IFxufSk7XG4gICAgJChcIi5zdGFja2VkLWl0ZW1cIikucGFyZW50KFwidWxcIikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgY29uc29sZS5sb2coJCh0aGlzKSk7XG4gICAgICAgIGNvbnNvbGUubG9nKG1lbnVIZWlnaHQpO1xuICAgICAgICAkKFwiLnN0YWNrZWQtaXRlbSAuZm9vIGxpXCIpLnJlbW92ZUF0dHIoXCJzdHlsZVwiKTtcbiAgICAgICAgJChcIi5zdGFja2VkLWl0ZW0gLmZvb1wiKS5jc3MoXCJjc3NUZXh0XCIsXCJwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudFwiKTtcbiAgICAgICAgJChcIi5zdGFja2VkLWl0ZW0gLmZvb1wiKS5jc3MoXCJsZWZ0XCIsXCIwXCIpO1xuICAgICAgICAkKFwiLnN0YWNrZWQtaXRlbSAuZm9vXCIpLmNzcyhcInBhZGRpbmdUb3BcIixtZW51SGVpZ2h0KTtcbiAgICAgICAvLyAkKFwiLnN0YWNrZWRcIikuY3NzKFwiaGVpZ2h0XCIsIG1lbnVIZWlnaHQgKyBzdGFja2VkSGVpZ2h0KVxuICAgIH0pO1xuICAgICQoIFwiLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW5cIiApLmhvdmVyKFxuICBmdW5jdGlvbigpIHtcbiAgICBjb25zb2xlLmxvZygkKHRoaXMpLnBhcmVudCgpKTtcbiAgICBsZXQgc3RhY2tlZEhlaWdodCA9ICQoXCIuc3RhY2tlZFwiKS5oZWlnaHQoKTtcbiAgICAgICAgJChcIi5zdGFja2VkXCIpLmNzcyhcImhlaWdodFwiLCBtZW51SGVpZ2h0ICsgc3RhY2tlZEhlaWdodCk7XG4gIH0sIGZ1bmN0aW9uKCkge1xuICAgIGxldCBzdGFja2VkSGVpZ2h0ID0gJChcIi5zdGFja2VkXCIpLmhlaWdodCgpO1xuICAgICAgICAkKFwiLnN0YWNrZWRcIikuY3NzKFwiaGVpZ2h0XCIsIHN0YWNrZWRIZWlnaHQgKTtcbiAgfVxuKTtcbiAgICAgIGNvbnNvbGUubG9nKCQoXCIudG9wLXN0cmlwZVwiKS5oZWlnaHQoKSk7XG4gICAgfTtcbiAgICAvLyBFbmdhZ2UhXG4gICAgJCggYXBwLmluaXQgKTtcbn0gKCB3aW5kb3csIGpRdWVyeSwgd2luZG93LndyTWVudURyb3BEb3duICkpO1xuIiwiLyoqXG4gKiBGaWxlOiBtb2JpbGUtbWVudS5qc1xuICpcbiAqIENyZWF0ZSBhbiBhY2NvcmRpb24gc3R5bGUgZHJvcGRvd24uXG4gKi9cbndpbmRvdy53ZHNNb2JpbGVNZW51ID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0Ym9keTogJCggJ2JvZHknICksXG5cdFx0XHR3aW5kb3c6ICQoIHdpbmRvdyApLFxuXHRcdFx0c3ViTWVudUNvbnRhaW5lcjogJCggJy5tb2JpbGUtbWVudSAuc3ViLW1lbnUsIC51dGlsaXR5LW5hdmlnYXRpb24gLnN1Yi1tZW51JyApLFxuXHRcdFx0c3ViU3ViTWVudUNvbnRhaW5lcjogJCggJy5tb2JpbGUtbWVudSAuc3ViLW1lbnUgLnN1Yi1tZW51JyApLFxuXHRcdFx0c3ViTWVudVBhcmVudEl0ZW06ICQoICcubW9iaWxlLW1lbnUgbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbiwgLnV0aWxpdHktbmF2aWdhdGlvbiBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCAnLm9mZi1jYW52YXMtY29udGFpbmVyJyApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuYWRkRG93bkFycm93ICk7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuYXBwZW5kV3JNZWdhICk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLm9uKCAnY2xpY2snLCBhcHAudG9nZ2xlU3VibWVudSApO1xuXHRcdGFwcC4kYy5zdWJNZW51UGFyZW50SXRlbS5vbiggJ3RyYW5zaXRpb25lbmQnLCBhcHAucmVzZXRTdWJNZW51ICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5vbiggJ3RyYW5zaXRpb25lbmQnLCBhcHAuZm9yY2VDbG9zZVN1Ym1lbnVzICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLnN1Yk1lbnVDb250YWluZXIubGVuZ3RoO1xuXHR9O1xuXG5cdC8vIFJlc2V0IHRoZSBzdWJtZW51cyBhZnRlciBpdCdzIGRvbmUgY2xvc2luZy5cblx0YXBwLnJlc2V0U3ViTWVudSA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gV2hlbiB0aGUgbGlzdCBpdGVtIGlzIGRvbmUgdHJhbnNpdGlvbmluZyBpbiBoZWlnaHQsXG5cdFx0Ly8gcmVtb3ZlIHRoZSBjbGFzc2VzIGZyb20gdGhlIHN1Ym1lbnUgc28gaXQgaXMgcmVhZHkgdG8gdG9nZ2xlIGFnYWluLlxuXHRcdGlmICggJCggdGhpcyApLmlzKCAnbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbicgKSAmJiAhICQoIHRoaXMgKS5oYXNDbGFzcyggJ2lzLXZpc2libGUnICkgKSB7XG5cdFx0XHQkKCB0aGlzICkuZmluZCggJ3VsLnN1Yi1tZW51JyApLnJlbW92ZUNsYXNzKCAnc2xpZGVPdXRMZWZ0IGlzLXZpc2libGUnICk7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gU2xpZGUgb3V0IHRoZSBzdWJtZW51IGl0ZW1zLlxuXHRhcHAuc2xpZGVPdXRTdWJNZW51cyA9IGZ1bmN0aW9uKCBlbCApIHtcblxuXHRcdC8vIElmIHRoaXMgaXRlbSdzIHBhcmVudCBpcyB2aXNpYmxlIGFuZCB0aGlzIGlzIG5vdCwgYmFpbC5cblx0XHRpZiAoIGVsLnBhcmVudCgpLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSAmJiAhIGVsLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHQvLyBJZiB0aGlzIGl0ZW0ncyBwYXJlbnQgaXMgdmlzaWJsZSBhbmQgdGhpcyBpdGVtIGlzIHZpc2libGUsIGhpZGUgaXRzIHN1Ym1lbnUgdGhlbiBiYWlsLlxuXHRcdGlmICggZWwucGFyZW50KCkuaGFzQ2xhc3MoICdpcy12aXNpYmxlJyApICYmIGVsLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblx0XHRcdGVsLnJlbW92ZUNsYXNzKCAnaXMtdmlzaWJsZScgKS5maW5kKCAnLnN1Yi1tZW51JyApLnJlbW92ZUNsYXNzKCAnc2xpZGVJbkxlZnQnICkuYWRkQ2xhc3MoICdzbGlkZU91dExlZnQnICk7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXG5cdFx0YXBwLiRjLnN1Yk1lbnVDb250YWluZXIuZWFjaCggZnVuY3Rpb24oKSB7XG5cblx0XHRcdC8vIE9ubHkgdHJ5IHRvIGNsb3NlIHN1Ym1lbnVzIHRoYXQgYXJlIGFjdHVhbGx5IG9wZW4uXG5cdFx0XHRpZiAoICQoIHRoaXMgKS5oYXNDbGFzcyggJ3NsaWRlSW5MZWZ0JyApICkge1xuXG5cdFx0XHRcdC8vIENsb3NlIHRoZSBwYXJlbnQgbGlzdCBpdGVtLCBhbmQgc2V0IHRoZSBjb3JyZXNwb25kaW5nIGJ1dHRvbiBhcmlhIHRvIGZhbHNlLlxuXHRcdFx0XHQkKCB0aGlzICkucGFyZW50KCkucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApLmZpbmQoICcucGFyZW50LWluZGljYXRvcicgKS5hdHRyKCAnYXJpYS1leHBhbmRlZCcsIGZhbHNlICk7XG5cblx0XHRcdFx0Ly8gU2xpZGUgb3V0IHRoZSBzdWJtZW51LlxuXHRcdFx0XHQkKCB0aGlzICkucmVtb3ZlQ2xhc3MoICdzbGlkZUluTGVmdCcgKS5hZGRDbGFzcyggJ3NsaWRlT3V0TGVmdCcgKTtcblx0XHRcdH1cblxuXHRcdH0gKTtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYWRkRG93bkFycm93ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLnByZXBlbmQoICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIiBjbGFzcz1cInBhcmVudC1pbmRpY2F0b3JcIiBhcmlhLWxhYmVsPVwiT3BlbiBzdWJtZW51XCI+PHNwYW4gY2xhc3M9XCJkb3duLWFycm93XCI+PC9zcGFuPjwvYnV0dG9uPicgKTtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYXBwZW5kV3JNZWdhID0gZnVuY3Rpb24oKSB7XG5cdFx0Y29uc29sZS5sb2coXCJ3ciBtZWdhbWVudSBnbyBoZXJlIVwiKTtcblx0fTtcblx0YXBwLmFwcGVuZFdyTWVnYTtcblx0Ly8gRGVhbCB3aXRoIHRoZSBzdWJtZW51LlxuXHRhcHAudG9nZ2xlU3VibWVudSA9IGZ1bmN0aW9uKCBlICkge1xuXG5cdFx0bGV0IGVsID0gJCggdGhpcyApLCAvLyBUaGUgbWVudSBlbGVtZW50IHdoaWNoIHdhcyBjbGlja2VkIG9uLlxuXHRcdFx0c3ViTWVudSA9IGVsLmNoaWxkcmVuKCAndWwuc3ViLW1lbnUnICksIC8vIFRoZSBuZWFyZXN0IHN1Ym1lbnUuXG5cdFx0XHQkdGFyZ2V0ID0gJCggZS50YXJnZXQgKTsgLy8gdGhlIGVsZW1lbnQgdGhhdCdzIGFjdHVhbGx5IGJlaW5nIGNsaWNrZWQgKGNoaWxkIG9mIHRoZSBsaSB0aGF0IHRyaWdnZXJlZCB0aGUgY2xpY2sgZXZlbnQpLlxuXG5cdFx0Ly8gRmlndXJlIG91dCBpZiB3ZSdyZSBjbGlja2luZyB0aGUgYnV0dG9uIG9yIGl0cyBhcnJvdyBjaGlsZCxcblx0XHQvLyBpZiBzbywgd2UgY2FuIGp1c3Qgb3BlbiBvciBjbG9zZSB0aGUgbWVudSBhbmQgYmFpbC5cblx0XHRpZiAoICR0YXJnZXQuaGFzQ2xhc3MoICdkb3duLWFycm93JyApIHx8ICR0YXJnZXQuaGFzQ2xhc3MoICdwYXJlbnQtaW5kaWNhdG9yJyApICkge1xuXG5cdFx0XHQvLyBGaXJzdCwgY29sbGFwc2UgYW55IGFscmVhZHkgb3BlbmVkIHN1Ym1lbnVzLlxuXHRcdFx0YXBwLnNsaWRlT3V0U3ViTWVudXMoIGVsICk7XG5cblx0XHRcdGlmICggISBzdWJNZW51Lmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblxuXHRcdFx0XHQvLyBPcGVuIHRoZSBzdWJtZW51LlxuXHRcdFx0XHRhcHAub3BlblN1Ym1lbnUoIGVsLCBzdWJNZW51ICk7XG5cblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIE9wZW4gYSBzdWJtZW51LlxuXHRhcHAub3BlblN1Ym1lbnUgPSBmdW5jdGlvbiggcGFyZW50LCBzdWJNZW51ICkge1xuXG5cdFx0Ly8gRXhwYW5kIHRoZSBsaXN0IG1lbnUgaXRlbSwgYW5kIHNldCB0aGUgY29ycmVzcG9uZGluZyBidXR0b24gYXJpYSB0byB0cnVlLlxuXHRcdHBhcmVudC5hZGRDbGFzcyggJ2lzLXZpc2libGUnICkuZmluZCggJy5wYXJlbnQtaW5kaWNhdG9yJyApLmF0dHIoICdhcmlhLWV4cGFuZGVkJywgdHJ1ZSApO1xuXG5cdFx0Ly8gU2xpZGUgdGhlIG1lbnUgaW4uXG5cdFx0c3ViTWVudS5hZGRDbGFzcyggJ2lzLXZpc2libGUgYW5pbWF0ZWQgc2xpZGVJbkxlZnQnICk7XG5cdH07XG5cblx0Ly8gRm9yY2UgY2xvc2UgYWxsIHRoZSBzdWJtZW51cyB3aGVuIHRoZSBtYWluIG1lbnUgY29udGFpbmVyIGlzIGNsb3NlZC5cblx0YXBwLmZvcmNlQ2xvc2VTdWJtZW51cyA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gVGhlIHRyYW5zaXRpb25lbmQgZXZlbnQgdHJpZ2dlcnMgb24gb3BlbiBhbmQgb24gY2xvc2UsIG5lZWQgdG8gbWFrZSBzdXJlIHdlIG9ubHkgZG8gdGhpcyBvbiBjbG9zZS5cblx0XHRpZiAoICEgJCggdGhpcyApLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblx0XHRcdGFwcC4kYy5zdWJNZW51UGFyZW50SXRlbS5yZW1vdmVDbGFzcyggJ2lzLXZpc2libGUnICkuZmluZCggJy5wYXJlbnQtaW5kaWNhdG9yJyApLmF0dHIoICdhcmlhLWV4cGFuZGVkJywgZmFsc2UgKTtcblx0XHRcdGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLnJlbW92ZUNsYXNzKCAnaXMtdmlzaWJsZSBzbGlkZUluTGVmdCcgKTtcblx0XHRcdGFwcC4kYy5ib2R5LmNzcyggJ292ZXJmbG93JywgJ3Zpc2libGUnICk7XG5cdFx0XHRhcHAuJGMuYm9keS51bmJpbmQoICd0b3VjaHN0YXJ0JyApO1xuXHRcdH1cblxuXHRcdGlmICggJCggdGhpcyApLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblx0XHRcdGFwcC4kYy5ib2R5LmNzcyggJ292ZXJmbG93JywgJ2hpZGRlbicgKTtcblx0XHRcdGFwcC4kYy5ib2R5LmJpbmQoICd0b3VjaHN0YXJ0JywgZnVuY3Rpb24oIGUgKSB7XG5cdFx0XHRcdGlmICggISAkKCBlLnRhcmdldCApLnBhcmVudHMoICcuY29udGFjdC1tb2RhbCcgKVswXSApIHtcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdH1cblx0XHRcdH0gKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc01vYmlsZU1lbnUgKSApO1xuXG5cdFx0XG5cdFx0alF1ZXJ5KHdpbmRvdykubG9hZChmdW5jdGlvbigpIHtcblx0XHQgdmFyIGlzX21vYmlsZV9icm93c2VyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiggbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvQW5kcm9pZC9pKVxuICAgICAgICAgICAgICAgIHx8IG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL3dlYk9TL2kpXG4gICAgICAgICAgICAgICAgfHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvaVBob25lL2kpXG4gICAgICAgICAgICAgICAgfHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvaVBhZC9pKVxuICAgICAgICAgICAgICAgIHx8IG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL2lQb2QvaSlcbiAgICAgICAgICAgICAgICB8fCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9CbGFja0JlcnJ5L2kpXG4gICAgICAgICAgICAgICAgfHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvV2luZG93cyBQaG9uZS9pKVxuICAgICAgICApIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgfTtcblx0XHRcdGlmIChpc19tb2JpbGVfYnJvd3Nlcikge1xuXHRcdC8valF1ZXJ5KCBcIi5vZmYtY2FudmFzLWNvbnRhaW5lclwiICkuYXBwZW5kKCBqUXVlcnkoXCIuY2VudGVyZWQtbWVudVwiKSApO1xuXHRcdGpRdWVyeSggXCIuY2VudGVyZWQtbWVudVwiICkuY2xvbmUoKS5hcHBlbmRUbyggalF1ZXJ5KFwiLm9mZi1jYW52YXMtY29udGFpbmVyXCIpICk7XG5cdFx0Ly9qUXVlcnkoIFwiLmNlbnRlcmVkLW1lbnVcIiApLmJlZm9yZSggalF1ZXJ5KFwiLmhvbWUtbG9nb1wiKSApO1xuXHRcdFx0XHRcblx0XHRcdH1cblx0XHQvL1RPRE8gcXVlcnkgY29uZGl0aW9uYWwsIGxvb2sgd3JtZWdhbWVudSBmb3IgZXhhbXBsZXMuXG59KTsiLCIvKipcbiAqIEZpbGUgbW9kYWwuanNcbiAqXG4gKiBEZWFsIHdpdGggbXVsdGlwbGUgbW9kYWxzIGFuZCB0aGVpciBtZWRpYS5cbiAqL1xud2luZG93Lndkc01vZGFsID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHRsZXQgJG1vZGFsVG9nZ2xlLFxuXHRcdCRmb2N1c2FibGVDaGlsZHJlbixcblx0XHQkcGxheWVyLFxuXHRcdCR0YWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCAnc2NyaXB0JyApLFxuXHRcdCRmaXJzdFNjcmlwdFRhZyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCAnc2NyaXB0JyApWzBdLFxuXHRcdFlUO1xuXG5cdC8vIENvbnN0cnVjdG9yLlxuXHRhcHAuaW5pdCA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKCBhcHAubWVldHNSZXF1aXJlbWVudHMoKSApIHtcblx0XHRcdCRmaXJzdFNjcmlwdFRhZy5wYXJlbnROb2RlLmluc2VydEJlZm9yZSggJHRhZywgJGZpcnN0U2NyaXB0VGFnICk7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0J2JvZHknOiAkKCAnYm9keScgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gJCggJy5tb2RhbC10cmlnZ2VyJyApLmxlbmd0aDtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBUcmlnZ2VyIGEgbW9kYWwgdG8gb3Blbi5cblx0XHRhcHAuJGMuYm9keS5vbiggJ2NsaWNrIHRvdWNoc3RhcnQnLCAnLm1vZGFsLXRyaWdnZXInLCBhcHAub3Blbk1vZGFsICk7XG5cblx0XHQvLyBUcmlnZ2VyIHRoZSBjbG9zZSBidXR0b24gdG8gY2xvc2UgdGhlIG1vZGFsLlxuXHRcdGFwcC4kYy5ib2R5Lm9uKCAnY2xpY2sgdG91Y2hzdGFydCcsICcuY2xvc2UnLCBhcHAuY2xvc2VNb2RhbCApO1xuXG5cdFx0Ly8gQWxsb3cgdGhlIHVzZXIgdG8gY2xvc2UgdGhlIG1vZGFsIGJ5IGhpdHRpbmcgdGhlIGVzYyBrZXkuXG5cdFx0YXBwLiRjLmJvZHkub24oICdrZXlkb3duJywgYXBwLmVzY0tleUNsb3NlICk7XG5cblx0XHQvLyBBbGxvdyB0aGUgdXNlciB0byBjbG9zZSB0aGUgbW9kYWwgYnkgY2xpY2tpbmcgb3V0c2lkZSBvZiB0aGUgbW9kYWwuXG5cdFx0YXBwLiRjLmJvZHkub24oICdjbGljayB0b3VjaHN0YXJ0JywgJ2Rpdi5tb2RhbC1vcGVuJywgYXBwLmNsb3NlTW9kYWxCeUNsaWNrICk7XG5cblx0XHQvLyBMaXN0ZW4gdG8gdGFicywgdHJhcCBrZXlib2FyZCBpZiB3ZSBuZWVkIHRvXG5cdFx0YXBwLiRjLmJvZHkub24oICdrZXlkb3duJywgYXBwLnRyYXBLZXlib2FyZE1heWJlICk7XG5cblx0fTtcblxuXHQvLyBPcGVuIHRoZSBtb2RhbC5cblx0YXBwLm9wZW5Nb2RhbCA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gU3RvcmUgdGhlIG1vZGFsIHRvZ2dsZSBlbGVtZW50XG5cdFx0JG1vZGFsVG9nZ2xlID0gJCggdGhpcyApO1xuXG5cdFx0Ly8gRmlndXJlIG91dCB3aGljaCBtb2RhbCB3ZSdyZSBvcGVuaW5nIGFuZCBzdG9yZSB0aGUgb2JqZWN0LlxuXHRcdGxldCAkbW9kYWwgPSAkKCAkKCB0aGlzICkuZGF0YSggJ3RhcmdldCcgKSApO1xuXG5cdFx0Ly8gRGlzcGxheSB0aGUgbW9kYWwuXG5cdFx0JG1vZGFsLmFkZENsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIEFkZCBib2R5IGNsYXNzLlxuXHRcdGFwcC4kYy5ib2R5LmFkZENsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIEZpbmQgdGhlIGZvY3VzYWJsZSBjaGlsZHJlbiBvZiB0aGUgbW9kYWwuXG5cdFx0Ly8gVGhpcyBsaXN0IG1heSBiZSBpbmNvbXBsZXRlLCByZWFsbHkgd2lzaCBqUXVlcnkgaGFkIHRoZSA6Zm9jdXNhYmxlIHBzZXVkbyBsaWtlIGpRdWVyeSBVSSBkb2VzLlxuXHRcdC8vIEZvciBtb3JlIGFib3V0IDppbnB1dCBzZWU6IGh0dHBzOi8vYXBpLmpxdWVyeS5jb20vaW5wdXQtc2VsZWN0b3IvXG5cdFx0JGZvY3VzYWJsZUNoaWxkcmVuID0gJG1vZGFsLmZpbmQoICdhLCA6aW5wdXQsIFt0YWJpbmRleF0nICk7XG5cblx0XHQvLyBJZGVhbGx5LCB0aGVyZSBpcyBhbHdheXMgb25lICh0aGUgY2xvc2UgYnV0dG9uKSwgYnV0IHlvdSBuZXZlciBrbm93LlxuXHRcdGlmICggMCA8ICRmb2N1c2FibGVDaGlsZHJlbi5sZW5ndGggKSB7XG5cblx0XHRcdC8vIFNoaWZ0IGZvY3VzIHRvIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudC5cblx0XHRcdCRmb2N1c2FibGVDaGlsZHJlblswXS5mb2N1cygpO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIENsb3NlIHRoZSBtb2RhbC5cblx0YXBwLmNsb3NlTW9kYWwgPSBmdW5jdGlvbigpIHtcblxuXHRcdC8vIEZpZ3VyZSB0aGUgb3BlbmVkIG1vZGFsIHdlJ3JlIGNsb3NpbmcgYW5kIHN0b3JlIHRoZSBvYmplY3QuXG5cdFx0bGV0ICRtb2RhbCA9ICQoICQoICdkaXYubW9kYWwtb3BlbiAuY2xvc2UnICkuZGF0YSggJ3RhcmdldCcgKSApLFxuXG5cdFx0XHQvLyBGaW5kIHRoZSBpZnJhbWUgaW4gdGhlICRtb2RhbCBvYmplY3QuXG5cdFx0XHQkaWZyYW1lID0gJG1vZGFsLmZpbmQoICdpZnJhbWUnICk7XG5cblx0XHQvLyBPbmx5IGRvIHRoaXMgaWYgdGhlcmUgYXJlIGFueSBpZnJhbWVzLlxuXHRcdGlmICggJGlmcmFtZS5sZW5ndGggKSB7XG5cblx0XHRcdC8vIEdldCB0aGUgaWZyYW1lIHNyYyBVUkwuXG5cdFx0XHRsZXQgdXJsID0gJGlmcmFtZS5hdHRyKCAnc3JjJyApO1xuXG5cdFx0XHQvLyBSZW1vdmluZy9SZWFkZGluZyB0aGUgVVJMIHdpbGwgZWZmZWN0aXZlbHkgYnJlYWsgdGhlIFlvdVR1YmUgQVBJLlxuXHRcdFx0Ly8gU28gbGV0J3Mgbm90IGRvIHRoYXQgd2hlbiB0aGUgaWZyYW1lIFVSTCBjb250YWlucyB0aGUgZW5hYmxlanNhcGkgcGFyYW1ldGVyLlxuXHRcdFx0aWYgKCAhIHVybC5pbmNsdWRlcyggJ2VuYWJsZWpzYXBpPTEnICkgKSB7XG5cblx0XHRcdFx0Ly8gUmVtb3ZlIHRoZSBzb3VyY2UgVVJMLCB0aGVuIGFkZCBpdCBiYWNrLCBzbyB0aGUgdmlkZW8gY2FuIGJlIHBsYXllZCBhZ2FpbiBsYXRlci5cblx0XHRcdFx0JGlmcmFtZS5hdHRyKCAnc3JjJywgJycgKS5hdHRyKCAnc3JjJywgdXJsICk7XG5cdFx0XHR9IGVsc2Uge1xuXG5cdFx0XHRcdC8vIFVzZSB0aGUgWW91VHViZSBBUEkgdG8gc3RvcCB0aGUgdmlkZW8uXG5cdFx0XHRcdCRwbGF5ZXIuc3RvcFZpZGVvKCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly8gRmluYWxseSwgaGlkZSB0aGUgbW9kYWwuXG5cdFx0JG1vZGFsLnJlbW92ZUNsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIFJlbW92ZSB0aGUgYm9keSBjbGFzcy5cblx0XHRhcHAuJGMuYm9keS5yZW1vdmVDbGFzcyggJ21vZGFsLW9wZW4nICk7XG5cblx0XHQvLyBSZXZlcnQgZm9jdXMgYmFjayB0byB0b2dnbGUgZWxlbWVudFxuXHRcdCRtb2RhbFRvZ2dsZS5mb2N1cygpO1xuXG5cdH07XG5cblx0Ly8gQ2xvc2UgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdGlmICggMjcgPT09IGV2ZW50LmtleUNvZGUgKSB7XG5cdFx0XHRhcHAuY2xvc2VNb2RhbCgpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDbG9zZSBpZiB0aGUgdXNlciBjbGlja3Mgb3V0c2lkZSBvZiB0aGUgbW9kYWxcblx0YXBwLmNsb3NlTW9kYWxCeUNsaWNrID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXG5cdFx0Ly8gSWYgdGhlIHBhcmVudCBjb250YWluZXIgaXMgTk9UIHRoZSBtb2RhbCBkaWFsb2cgY29udGFpbmVyLCBjbG9zZSB0aGUgbW9kYWxcblx0XHRpZiAoICEgJCggZXZlbnQudGFyZ2V0ICkucGFyZW50cyggJ2RpdicgKS5oYXNDbGFzcyggJ21vZGFsLWRpYWxvZycgKSApIHtcblx0XHRcdGFwcC5jbG9zZU1vZGFsKCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIFRyYXAgdGhlIGtleWJvYXJkIGludG8gYSBtb2RhbCB3aGVuIG9uZSBpcyBhY3RpdmUuXG5cdGFwcC50cmFwS2V5Ym9hcmRNYXliZSA9IGZ1bmN0aW9uKCBldmVudCApIHtcblxuXHRcdC8vIFdlIG9ubHkgbmVlZCB0byBkbyBzdHVmZiB3aGVuIHRoZSBtb2RhbCBpcyBvcGVuIGFuZCB0YWIgaXMgcHJlc3NlZC5cblx0XHRpZiAoIDkgPT09IGV2ZW50LndoaWNoICYmIDAgPCAkKCAnLm1vZGFsLW9wZW4nICkubGVuZ3RoICkge1xuXHRcdFx0bGV0ICRmb2N1c2VkID0gJCggJzpmb2N1cycgKSxcblx0XHRcdFx0Zm9jdXNJbmRleCA9ICRmb2N1c2FibGVDaGlsZHJlbi5pbmRleCggJGZvY3VzZWQgKTtcblxuXHRcdFx0aWYgKCAwID09PSBmb2N1c0luZGV4ICYmIGV2ZW50LnNoaWZ0S2V5ICkge1xuXG5cdFx0XHRcdC8vIElmIHRoaXMgaXMgdGhlIGZpcnN0IGZvY3VzYWJsZSBlbGVtZW50LCBhbmQgc2hpZnQgaXMgaGVsZCB3aGVuIHByZXNzaW5nIHRhYiwgZ28gYmFjayB0byBsYXN0IGZvY3VzYWJsZSBlbGVtZW50LlxuXHRcdFx0XHQkZm9jdXNhYmxlQ2hpbGRyZW5bICRmb2N1c2FibGVDaGlsZHJlbi5sZW5ndGggLSAxIF0uZm9jdXMoKTtcblx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdH0gZWxzZSBpZiAoICEgZXZlbnQuc2hpZnRLZXkgJiYgZm9jdXNJbmRleCA9PT0gJGZvY3VzYWJsZUNoaWxkcmVuLmxlbmd0aCAtIDEgKSB7XG5cblx0XHRcdFx0Ly8gSWYgdGhpcyBpcyB0aGUgbGFzdCBmb2N1c2FibGUgZWxlbWVudCwgYW5kIHNoaWZ0IGlzIG5vdCBoZWxkLCBnbyBiYWNrIHRvIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudC5cblx0XHRcdFx0JGZvY3VzYWJsZUNoaWxkcmVuWzBdLmZvY3VzKCk7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xuXG5cdC8vIEhvb2sgaW50byBZb3VUdWJlIDxpZnJhbWU+LlxuXHRhcHAub25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkgPSBmdW5jdGlvbigpIHtcblx0XHRsZXQgJG1vZGFsID0gJCggJ2Rpdi5tb2RhbCcgKSxcblx0XHRcdCRpZnJhbWVpZCA9ICRtb2RhbC5maW5kKCAnaWZyYW1lJyApLmF0dHIoICdpZCcgKTtcblxuXHRcdCRwbGF5ZXIgPSBuZXcgWVQuUGxheWVyKCAkaWZyYW1laWQsIHtcblx0XHRcdGV2ZW50czoge1xuXHRcdFx0XHQnb25SZWFkeSc6IGFwcC5vblBsYXllclJlYWR5LFxuXHRcdFx0XHQnb25TdGF0ZUNoYW5nZSc6IGFwcC5vblBsYXllclN0YXRlQ2hhbmdlXG5cdFx0XHR9XG5cdFx0fSApO1xuXHR9O1xuXG5cdC8vIERvIHNvbWV0aGluZyBvbiBwbGF5ZXIgcmVhZHkuXG5cdGFwcC5vblBsYXllclJlYWR5ID0gZnVuY3Rpb24oKSB7XG5cdH07XG5cblx0Ly8gRG8gc29tZXRoaW5nIG9uIHBsYXllciBzdGF0ZSBjaGFuZ2UuXG5cdGFwcC5vblBsYXllclN0YXRlQ2hhbmdlID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBTZXQgZm9jdXMgdG8gdGhlIGZpcnN0IGZvY3VzYWJsZSBlbGVtZW50IGluc2lkZSBvZiB0aGUgbW9kYWwgdGhlIHBsYXllciBpcyBpbi5cblx0XHQkKCBldmVudC50YXJnZXQuYSApLnBhcmVudHMoICcubW9kYWwnICkuZmluZCggJ2EsIDppbnB1dCwgW3RhYmluZGV4XScgKS5maXJzdCgpLmZvY3VzKCk7XG5cdH07XG5cblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc01vZGFsICkgKTtcbiIsIi8qKlxuICogRmlsZTogbmF2aWdhdGlvbi1wcmltYXJ5LmpzXG4gKlxuICogSGVscGVycyBmb3IgdGhlIHByaW1hcnkgbmF2aWdhdGlvbi5cbiAqL1xud2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdHN1Yk1lbnVDb250YWluZXI6ICQoICcubWFpbi1uYXZpZ2F0aW9uIC5zdWItbWVudScgKSxcblx0XHRcdHN1Yk1lbnVQYXJlbnRJdGVtOiAkKCAnLm1haW4tbmF2aWdhdGlvbiBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuYWRkRG93bkFycm93ICk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoICdhJyApLm9uKCAnZm9jdXNpbiBmb2N1c291dCcsIGFwcC50b2dnbGVGb2N1cyApO1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLmxlbmd0aDtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYWRkRG93bkFycm93ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoICc+IGEnICkuYXBwZW5kKCAnPHNwYW4gY2xhc3M9XCJjYXJldC1kb3duXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9zcGFuPicgKTtcblx0fTtcblxuXHQvLyBUb2dnbGUgdGhlIGZvY3VzIGNsYXNzIG9uIHRoZSBsaW5rIHBhcmVudC5cblx0YXBwLnRvZ2dsZUZvY3VzID0gZnVuY3Rpb24oKSB7XG5cdFx0JCggdGhpcyApLnBhcmVudHMoICdsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApLnRvZ2dsZUNsYXNzKCAnZm9jdXMnICk7XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uICkgKTtcbiIsIi8qKlxuICogRmlsZTogb2ZmLWNhbnZhcy5qc1xuICpcbiAqIEhlbHAgZGVhbCB3aXRoIHRoZSBvZmYtY2FudmFzIG1vYmlsZSBtZW51LlxuICovXG53aW5kb3cud2Rzb2ZmQ2FudmFzID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0Ym9keTogJCggJ2JvZHknICksXG5cdFx0XHRvZmZDYW52YXNDbG9zZTogJCggJy5vZmYtY2FudmFzLWNsb3NlJyApLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCAnLm9mZi1jYW52YXMtY29udGFpbmVyJyApLFxuXHRcdFx0b2ZmQ2FudmFzT3BlbjogJCggJy5vZmYtY2FudmFzLW9wZW4nICksXG5cdFx0XHRvZmZDYW52YXNTY3JlZW46ICQoICcub2ZmLWNhbnZhcy1zY3JlZW4nIClcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50cy5cblx0YXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMuYm9keS5vbiggJ2tleWRvd24nLCBhcHAuZXNjS2V5Q2xvc2UgKTtcblx0XHRhcHAuJGMub2ZmQ2FudmFzQ2xvc2Uub24oICdjbGljaycsIGFwcC5jbG9zZW9mZkNhbnZhcyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNPcGVuLm9uKCAnY2xpY2snLCBhcHAudG9nZ2xlb2ZmQ2FudmFzICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc1NjcmVlbi5vbiggJ2NsaWNrJywgYXBwLmNsb3Nlb2ZmQ2FudmFzICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5sZW5ndGg7XG5cdH07XG5cblx0Ly8gVG8gc2hvdyBvciBub3QgdG8gc2hvdz9cblx0YXBwLnRvZ2dsZW9mZkNhbnZhcyA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0aWYgKCAndHJ1ZScgPT09ICQoIHRoaXMgKS5hdHRyKCAnYXJpYS1leHBhbmRlZCcgKSApIHtcblx0XHRcdGFwcC5jbG9zZW9mZkNhbnZhcygpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRhcHAub3Blbm9mZkNhbnZhcygpO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIFNob3cgdGhhdCBkcmF3ZXIhXG5cdGFwcC5vcGVub2ZmQ2FudmFzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hZGRDbGFzcyggJ2lzLXZpc2libGUnICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYWRkQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4uYWRkQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCB0cnVlICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hdHRyKCAnYXJpYS1oaWRkZW4nLCBmYWxzZSApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5maW5kKCAnYnV0dG9uJyApLmZpcnN0KCkuZm9jdXMoKTtcblx0fTtcblxuXHQvLyBDbG9zZSB0aGF0IGRyYXdlciFcblx0YXBwLmNsb3Nlb2ZmQ2FudmFzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5yZW1vdmVDbGFzcyggJ2lzLXZpc2libGUnICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNDb250YWluZXIuYXR0ciggJ2FyaWEtaGlkZGVuJywgdHJ1ZSApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uZm9jdXMoKTtcblx0fTtcblxuXHQvLyBDbG9zZSBkcmF3ZXIgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdGlmICggMjcgPT09IGV2ZW50LmtleUNvZGUgKSB7XG5cdFx0XHRhcHAuY2xvc2VvZmZDYW52YXMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc29mZkNhbnZhcyApICk7XG4iLCJ3aW5kb3cuRWhTaG9wID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCkge1xuXG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJy53b29mX3Byb2R1Y3RzX3RvcF9wYW5lbCcpLmNzcygnZGlzcGxheScsJ25vbmUnKTtcbiAgICAgICAgXG5cdC8vXHRhcHAuc2xpZGVJbml0KCcuc2xpZGUtYmVzdCcpO1xuXG4gICAgfTtcbiAgICBhcHAuc2xpZGVJbml0ID0gZnVuY3Rpb24odG9TbGlkZSkge1xuICAgICAgICBcbiAgICB9XG4gICAgLy8gRW5nYWdlIVxuICAgICQoIGFwcC5pbml0ICk7XG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy5FaFNob3AgKSk7XG4iLCIvKipcbiAqIEZpbGUgc2tpcC1saW5rLWZvY3VzLWZpeC5qcy5cbiAqXG4gKiBIZWxwcyB3aXRoIGFjY2Vzc2liaWxpdHkgZm9yIGtleWJvYXJkIG9ubHkgdXNlcnMuXG4gKlxuICogTGVhcm4gbW9yZTogaHR0cHM6Ly9naXQuaW8vdldkcjJcbiAqL1xuKCBmdW5jdGlvbigpIHtcblx0dmFyIGlzV2Via2l0ID0gLTEgPCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ3dlYmtpdCcgKSxcblx0XHRpc09wZXJhID0gLTEgPCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ29wZXJhJyApLFxuXHRcdGlzSWUgPSAtMSA8IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCAnbXNpZScgKTtcblxuXHRpZiAoICggaXNXZWJraXQgfHwgaXNPcGVyYSB8fCBpc0llICkgJiYgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgKSB7XG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdoYXNoY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgaWQgPSBsb2NhdGlvbi5oYXNoLnN1YnN0cmluZyggMSApLFxuXHRcdFx0XHRlbGVtZW50O1xuXG5cdFx0XHRpZiAoICEgKCAvXltBLXowLTlfLV0rJC8gKS50ZXN0KCBpZCApICkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggaWQgKTtcblxuXHRcdFx0aWYgKCBlbGVtZW50ICkge1xuXHRcdFx0XHRpZiAoICEgKCAvXig/OmF8c2VsZWN0fGlucHV0fGJ1dHRvbnx0ZXh0YXJlYSkkL2kgKS50ZXN0KCBlbGVtZW50LnRhZ05hbWUgKSApIHtcblx0XHRcdFx0XHRlbGVtZW50LnRhYkluZGV4ID0gLTE7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRlbGVtZW50LmZvY3VzKCk7XG5cdFx0XHR9XG5cdFx0fSwgZmFsc2UgKTtcblx0fVxufSgpICk7XG4iLCJ3aW5kb3cuc2xpY2tFaHNob3AgPSB7fTtcbiggZnVuY3Rpb24oIHdpbmRvdywgJCwgYXBwKSB7XG5cblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuc2xpZGVJbml0KCcuc2xpZGUtYmVzdCcpO1xuXG4gICAgfTtcbiAgICBhcHAuc2xpZGVJbml0ID0gZnVuY3Rpb24odG9TbGlkZSkge1xuICAgICAgICAkKHRvU2xpZGUpLnNsaWNrKHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgc2xpZGVzVG9TaG93OiA2LFxuICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgICAgICBhdXRvcGxheTogZmFsc2UsXG4gICAgICAgICAgICBhdXRvcGxheVNwZWVkOiAyMDAwLFxuICAgICAgICAgICAgcmVzcG9uc2l2ZTogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogNDAwLFxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvdHM6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBZb3UgY2FuIHVuc2xpY2sgYXQgYSBnaXZlbiBicmVha3BvaW50IG5vdyBieSBhZGRpbmc6XG4gICAgICAgICAgICAgICAgLy8gc2V0dGluZ3M6IFwidW5zbGlja1wiXG4gICAgICAgICAgICAgICAgLy8gaW5zdGVhZCBvZiBhIHNldHRpbmdzIG9iamVjdFxuICAgICAgICAgICAgXVxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgfVxuLyogICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gICAgICAgICQoJy5zbGlkZS1iZXN0Jykuc2xpY2soe1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDYsXG4gICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgICAgICAgICAgYXV0b3BsYXlTcGVlZDogMjAwMCxcbiAgICAgICAgfSk7XG4gICAgfSk7ICovXG4gICAgLy8gRW5nYWdlIVxuICAgICQoIGFwcC5pbml0ICk7XG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy5zbGlja0Voc2hvcCApKTtcbiIsIi8qKlxuICogRmlsZSB3aW5kb3ctcmVhZHkuanNcbiAqXG4gKiBBZGQgYSBcInJlYWR5XCIgY2xhc3MgdG8gPGJvZHk+IHdoZW4gd2luZG93IGlzIHJlYWR5LlxuICovXG53aW5kb3cud2RzV2luZG93UmVhZHkgPSB7fTtcbiggZnVuY3Rpb24oIHdpbmRvdywgJCwgYXBwICkge1xuXG5cdC8vIENvbnN0cnVjdG9yLlxuXHRhcHAuaW5pdCA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXHRcdGFwcC5iaW5kRXZlbnRzKCk7XG5cdH07XG5cblx0Ly8gQ2FjaGUgZG9jdW1lbnQgZWxlbWVudHMuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdCd3aW5kb3cnOiAkKCB3aW5kb3cgKSxcblx0XHRcdCdib2R5JzogJCggZG9jdW1lbnQuYm9keSApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLndpbmRvdy5sb2FkKCBhcHAuYWRkQm9keUNsYXNzICk7XG5cdH07XG5cblx0Ly8gQWRkIGEgY2xhc3MgdG8gPGJvZHk+LlxuXHRhcHAuYWRkQm9keUNsYXNzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLmJvZHkuYWRkQ2xhc3MoICdyZWFkeScgKTtcblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc1dpbmRvd1JlYWR5ICkgKTtcbiJdfQ==
