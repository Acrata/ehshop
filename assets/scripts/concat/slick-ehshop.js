window.slickEhshop = {};
( function( window, $, app) {


	// Constructor.
	app.init = function() {
		app.slideInit('.slide-best');

    };
    app.slideInit = function(toSlide) {
        $(toSlide).slick({
            
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
        
    }
/*     $(document).ready(function(){
        $('.slide-best').slick({
            
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        });
    }); */
    // Engage!
    $( app.init );
} ( window, jQuery, window.slickEhshop ));
