<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
		
		<?php if ( have_posts() ) : ?>

<?php if ( is_active_sidebar( 'yith_2' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
    ypf
		<?php dynamic_sidebar( 'yith_2' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>
			<header class="page-header">
				<?php
				//	the_archive_title( '<h1 class="page-title">', '</h1>' );
			//		the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->
			<div id="archive-eh">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
				//get_template_part( 'template-parts/content', get_post_format() );
				
				get_template_part( 'template-parts/content', 'archive-filter' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		wp_reset_query();
		?>
		</div>
		</main><!-- #main -->
	</div><!-- .primary -->
<?php get_footer(); ?>
