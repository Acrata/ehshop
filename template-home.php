<?php
/**
 * Template Name: Homepage EcoHealthy
 *
 * The template for displaying pages with ACF components.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */
?>
	 		<?php $mapurite =  myprefix_get_option( 'top_stripe_text' ); ?>
	 		<div class="top-stripe"><?php echo  $mapurite; ?></div>
<style>
.hero-image-eh {
	 background: url("<?php echo get_post_meta($post->ID, '_ehshop_hero_image', true);?>")  center no-repeat / cover; 
	 }

</style>
<?php
	$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>
<section class="hero-image-eh"></section>
		<img class="home-logo" src="<?php echo $image[0];?>" alt="">	
<?php //} ?>
<?php get_header(); ?>

	<div class="content-area">
		<main id="main" class="site-main">
			<?php	get_template_part( 'template-parts/content', 'product-grid' ); ?>
			<?php	//get_template_part( 'template-parts/content', 'best-sellers-proto' ); ?>
			<?php	get_template_part( 'template-parts/content', 'best-sellers' ); ?>
			<?php	get_template_part( 'template-parts/content', 'followus-ig' ); ?>
			<?php	//get_template_part( 'template-parts/content', 'services-proto' ); ?>
			<?php	get_template_part( 'template-parts/content', 'services' ); ?>
			<?php	get_template_part( 'template-parts/content', 'contact-section-proto' ); ?>
			<?php	get_template_part( 'template-parts/content', 'contact-section' ); ?>
			<?php	get_template_part( 'template-parts/content', 'footer-links' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
