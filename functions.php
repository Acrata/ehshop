<?php
/**
 * eh-shop functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eh-shop
 */

if ( ! function_exists( 'ehshop_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ehshop_setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on eh-shop, use a find and replace
		 * to change 'ehshop' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'ehshop', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'full-width', 1920, 1080, false );

		// Register navigation menus.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'ehshop' ),
			'mobile'  => esc_html__( 'Mobile Menu', 'ehshop' ),
			'left'  => esc_html__( 'Left Menu', 'ehshop' ),
			'right'  => esc_html__( 'Right Menu', 'ehshop' ),
			'widget-nav'  => esc_html__( 'Widgets Menu', 'ehshop' ),
		) );

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ehshop_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Custom logo support.
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 500,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // ehshop_setup
add_action( 'after_setup_theme', 'ehshop_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ehshop_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ehshop_content_width', 640 );
}
add_action( 'after_setup_theme', 'ehshop_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ehshop_widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1'  => esc_html__( 'Sidebar 1', 'ehshop' ),
		// 'sidebar-2'  => esc_html__( 'Sidebar 2', 'ehshop' ),
		// 'sidebar-3'  => esc_html__( 'Sidebar 3', 'ehshop' ),
		 'sidebar-YPF'  => esc_html__( 'Sidebar YITH', 'ehshop' ),
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar( array(
			'name'          => $sidebar_name,
			'id'            => $sidebar_id,
			'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'ehshop' ), $sidebar_name ),
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}

}
add_action( 'widgets_init', 'ehshop_widgets_init' );


/**	
 * Yith widget
 */


/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'SIDE YITH 2',
		'id'            => 'yith_2',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/metaboxes/metaboxes.php';

/**
 * Hector's code starts here
 */

 /**
  * Add images sizes
  */
	add_image_size( 'menu-image', 280, 300, false );



	/**
	 * Customizer menu logo
	 */

/**
 * Icon cart menu
 */

 /**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {

    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php
    }
        ?></a><?php

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
wp_enqueue_style( 'dashicons' );
}

add_image_size( 'ig-thumb', 1080, 1080, true ); 


function my_enqueue_files() {
  wp_enqueue_script( 'wp-api' );
}
add_action( 'admin_enqueue_scripts', 'my_enqueue_files' );

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'thumbnail_image_width' => 350,
		'single_image_width'    => 800,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 3,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
	) );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
//add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

   add_action( 'woocommerce_after_shop_loop_item', 'remove_add_to_cart_buttons', 1 );

    function remove_add_to_cart_buttons() {
      if( is_product_category() || is_shop()) { 
        remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
      }
    }

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);



function show_sitemap() {
    $the_query = new WP_Query(array('post_type' => 'any', 'posts_per_page' => '-1', 'post_status' => 'publish'));
    $urls = array();
    while($the_query->have_posts()) {
      $the_query->the_post();
      $urls[] = get_permalink();
	}
    //die(json_encode($urls));
		return json_encode($urls);
}
//add_action('template_redirect', 'show_sitemap');
/* 
function crunchify_print_scripts_styles() {
    // Print all loaded Scripts
    global $wp_scripts;
    foreach( $wp_scripts->queue as $script ) :
        echo $script . '  **  ';
    endforeach;
 
    // Print all loaded Styles (CSS)
    global $wp_styles;
    foreach( $wp_styles->queue as $style ) :
        echo $style . '  ||  ';
    endforeach;
}
 
add_action( 'wp_print_scripts', 'crunchify_print_scripts_styles' ); */

/* $resultado = show_sitemap();
echo "<pre>";
print_r($resultado);
echo "</pre>";
echo "<br>";
echo TEMPLATEPATH;
echo "<br>";
echo get_template_directory(); */

add_action( 'woocommerce_before_shop_loop', 'bbloomer_custom_action', 15 );
 
function bbloomer_custom_action() {

echo do_shortcode("[woof sid='shop-filter']");
}

// define the woocommerce_after_shop_loop_item_title callback 
function action_woocommerce_after_shop_loop_item_title(  ) { 
 echo '<p>TEST</p>';
    // make action magic happen here... 
}; 
         
// add the action 
add_action( 'woocommerce_after_shop_loop_item_title', 'action_woocommerce_after_shop_loop_item_title', 10, 0 ); 


/**
 * Add a custom product data tab
 */
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	
	// Adds the new tab
	
	$tabs['test_tab'] = array(
		'title' 	=> __( 'New Product Tab test', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_new_product_tab_content'
	);

	return $tabs;

}
function woo_new_product_tab_content() {

	// The new tab content

	echo '<h2>New Product Tab</h2>';
	echo '<p>Here\'s your new product tab.</p>';
	
}

add_action( 'woocommerce_before_add_to_cart_button', 'bbloomer_custom_actionx', 5 );
 
function bbloomer_custom_actionx() {
echo 'TEST';
}

add_action( 'woocommerce_before_single_variation', 'bbloomer_custom_actionss', 5 );
 
function bbloomer_custom_actionss() {
echo 'TEST vari';
}

add_filter( 'woocommerce_cart_item_name', 'cart_variation_description', 20, 3);
function cart_variation_description( $name, $cart_item, $cart_item_key ) {
    // Get the corresponding WC_Product
    $product_item = $cart_item['data'];

    if(!empty($product_item) && $product_item->is_type( 'variation' ) ) {
        // WC 3+ compatibility
        $descrition = version_compare( WC_VERSION, '3.0', '<' ) ? $product_item->get_variation_description() : $product_item->get_description();
        $result = __( 'Description: ', 'woocommerce' ) . $descrition;
        return $name . '<br>' . $result;
    } else
        return $name;
}

// define the woocommerce_cart_item_name callback 
function filter_woocommerce_cart_item_name( $product_get_name, $cart_item, $cart_item_key ) { 
	// make filter magic happen here... 
	//print_r($cart_item);
    return $product_get_name; 
}; 
         
// add the filter 
add_filter( 'woocommerce_cart_item_name', 'filter_woocommerce_cart_item_name', 10, 3 ); 

// define the woocommerce_dropdown_variation_attribute_options_html callback 
function filter_woocommerce_dropdown_variation_attribute_options_html( $html, $args ) { 
	// make filter magic happen here... 
    return $html; 
}; 
         
// add the filter 
//add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'filter_woocommerce_dropdown_variation_attribute_options_html', 10, 2 ); 