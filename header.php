<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eh-shop
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700,800" rel="stylesheet">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'ehshop' ); ?></a>

	<header id="header" class="site-header cd-secondary-nav">

		<?php  
    if ( !is_front_page() ) {
		?>
		<div class="nav-top-ehshop">
			<div class="social-top-eh">
				<span class="dashicons dashicons-portfolio"></span>
				<span class="dashicons dashicons-phone"></span>
				<span class="dashicons dashicons-megaphone"></span>
				<span class="dashicons dashicons-info"></span>
			</div>
			<?php the_custom_logo(); ?>
			<div class="social-top-eh">
				<span class="dashicons dashicons-portfolio"></span>
				<span class="dashicons dashicons-phone"></span>
				<span class="dashicons dashicons-megaphone"></span>
				<span class="dashicons dashicons-info"></span>
			</div>
		</div>
		<?php
    }
    ?>
		<?php //ehshop_display_header_button(); ?>

		<button type="button" class="off-canvas-open" aria-expanded="false" aria-label="<?php esc_html_e( 'Open Menu', 'ehshop' ); ?>">
			<span class="hamburger"></span>
		</button>
		<nav class="centered-menu">
			<?php
	wp_nav_menu( array(
		'theme_location' => 'left',
		'menu_id'        => 'left-menu',
		'menu_class'     => 'menu dropdown',
	) );
?>
			<?php the_custom_logo(); ?>
<?php
	wp_nav_menu( array(
		'theme_location' => 'right',
		'menu_id'        => 'right-menu',
		'menu_class'     => 'menu dropdown',
	) );
?>

<div id="widget-nav-eh">
<?php
	wp_nav_menu( array(
		'theme_location' => 'widget-nav',
		'menu_id'        => 'widget-menu',
		'menu_class'     => 'menu dropdown',
	) );
?>
</div>
		</nav>
		
		<div class="icons-eh-shop">
			<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

				$count = WC()->cart->cart_contents_count;
				?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
				if ( $count > 0 ) {
					?>
					<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
					<?php
				}
				?></a>

			<?php } ?>
			<a href="#" class="dashicons dashicons-search"></a>
		</div>

	</header><!-- .site-header-->

	<div id="content" class="site-contenta">
