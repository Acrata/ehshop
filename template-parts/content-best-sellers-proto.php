<?php
/**
 * Template part for displaying the password protection form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

?>
<section class="best-sellers">
    <h2>ECOHEALTHY BEST SELLERS PROTO</h2>
    <div class="slide-best">
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/ancient-2704776_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Muuto</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/accessories-1347846_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Vitra</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/ancient-2704776_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Marset</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/accessories-1347846_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Norman Copenhagen</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/ancient-2704776_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Strings</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/accessories-1347846_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Lumio</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
        <div class="best-item"><a href="#">
            <img src="http://127.0.0.1/server/ehshop/wp-content/uploads/2018/05/accessories-1347846_1920.jpg" alt=""></a>
            <span class="item-info">
                <p>Muuto</p>
                <p>Item name</p>
                <p>13.99€</p>
            </span>
        </div>
    </div>
</section>
