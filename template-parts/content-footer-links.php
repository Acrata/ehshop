<?php
/**
 * Template part for displaying the password protection form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

?>
<section class="footer-links">
    <ul>
        <li>&Tradition</li>
        <li>Adico</li>
        <li>Aesop</li>
        <li>Alessi</li>
        <li>Alki</li>
        <li>Andersen</li>
        <li>Adreu World</li>
        <li>Anglepoise</li>
    </ul>
    <ul>
        <li>Casamaia</li>
        <li>Case</li>
        <li>Chener Chier Company</li>
        <li>Closca</li>
        <li>Cole & Son</li>
        <li>Colos</li>
        <li>DCW-Lampe-Gras</li>
        <li>DCW-Éditions</li>
    </ul>
    <ul>
        <li>Ercol</li>
        <li>Erik Jorgensen</li>
        <li>Ethinicraft</li>
        <li>Fern Living</li>
        <li>Fermob</li>
        <li>Fjordfiesta</li>
        <li>Fontanaarte</li>
        <li>Fredericia</li>
    </ul>
    <ul>
        <li>Joquer</li>
        <li>KREAFUNK</li>
        <li>Kartell</li>
        <li>Kettal</li>
        <li>Kikkerland </li>
        <li>Knoll </li>
        <li>Komono </li>
        <li>Kvadrat</li>
    </ul>
    <ul>
        <li>Mater</li>
        <li>Mattiazzi</li>
        <li>Menu</li>
        <li>Miniaturas VITRA</li>
        <li>Miras Editions</li>
        <li>Mobles 114</li>
        <li>Modus</li>
        <li>Moooi</li>
    </ul>
    <ul>
        <li>Ondaretta</li>
        <li>PET Lamp</li>
        <li>Paper Collective</li>
        <li>Papier Tigre</li>
        <li>Pappelina</li>
        <li>Pastoe</li>
        <li>Picto</li>
        <li>Plank</li>
    </ul>
    <ul>
        <li>Serax</li>
        <li>Serif TV Samsung</li>
        <li>Sika-Design</li>
        <li>Skargaarden</li>
        <li>Soft Line</li>
        <li>Spring Copenhagen</li>
        <li>Stadler Form</li>
        <li>Steiton</li>
    </ul>
    <ul>
        <li>Verpan</li>
        <li>Vibia</li>
        <li>Viccarbe</li>
        <li>Vifa</li>
        <li>Vitra</li>
        <li>Vlaemynck</li>
        <li>Woud</li>
        <li>Wouf</li>
    </ul>
</section>
