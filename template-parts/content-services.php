<?php
/**
 * Template part for displaying the password protection form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

?>
<?php
$section_services_title = get_post_meta($post->ID, '_ehshop_service_section_title', true);
$services_group = get_post_meta($post->ID, '_ehshop_services_group', true);
?>
<section class="services-eh">
    <h2><?php echo $section_services_title; ?></h2>
    <div class="service-list">
    <?php foreach ($services_group as $item) { ?>
        <div class="service-item" style="background:<?php echo $item['_ehshop_service_bg_colorpicker'];?>">
            <h3><?php echo $item['_ehshop_service_title']; ?></h3>
        </div>
    <?php } ?>
    </div>
</section>
