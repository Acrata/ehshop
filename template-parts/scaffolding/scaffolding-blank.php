<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package eh-shop
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'ehshop' ); ?></h2>
</section>
