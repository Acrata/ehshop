<?php
/**
 * Template part for displaying the password protection form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

?>
<?php
$section_bs_title = get_post_meta($post->ID, '_ehshop_best_seller_group_title', true); 
$best_seller_group = get_post_meta($post->ID, '_ehshop_best_seller_group', true);
?>
<section class="best-sellers">
    <h2><?php echo $section_bs_title; ?></h2>
    <div class="slide-best">
    <?php foreach ($best_seller_group as $item) { ?>
        <div class="best-item"><a href="#">
            <img src="<?php echo esc_html($item['_ehshop_image_bs']); ?>" alt=""></a>
            <span class="item-info">
                <p><?php echo $item['_ehshop_item_brand_bs']; ?></p>
                <p><?php echo $item['_ehshop_item_title_bs']; ?></p>
                <p><?php echo $item['_ehshop_item_price_bs']; ?></p>
            </span>
        </div>
    <?php } ?>
</div>
</section>
