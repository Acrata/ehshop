<?php
/**
 * Template part for displaying the password protection form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

?>
<section class="services-eh">
    <h2>NUESTRA PROFESIONALIDAD A TU SERVICIO PROTO</h2>
    <div class="service-list">
        <div class="service-item">
            <h3>ESTUDIO DE FENG SHUI</h3>
        </div>
        <div class="service-item">
            <h3>ESTUDIO BIOFILICO</h3>
        </div>
        <div class="service-item">
            <h3>DISEÑO PERSONALIZADO</h3>
        </div>
    </div>
</section>
