<?php
/**
 * Template part for displaying the password protection form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eh-shop
 */

?>
<div id="product-grid">
<?php 
    $img_items = get_post_meta($post->ID, '_ehshop_product_image', true); 
    if ($img_items) {
    foreach ($img_items as $item ) {
   
	?>
    <div class="menu-item" style=background:url("<?php echo esc_html($item['_ehshop_product_image_it']);?>");><span><?php echo $item['_ehshop_title_product']?></span></div>
   
<?php
}
    } else {
        echo "Warn!";
    }
?>
</div>