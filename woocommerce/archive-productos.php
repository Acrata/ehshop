<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php //woocommerce_page_title(); ?></h1>
	<?php endif; ?>
	<?php
global $wp_scripts, $wp_styles;
  //echo '<pre>';
  //var_dump( $wp_scripts );
  //echo '</pre>';
  ?>
	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>

<?php 
// verify that this is a product category page
if (is_product_category()){
	global $wp_query;
    // get the query object
    $cat = $wp_query->get_queried_object();
    // get the thumbnail id user the term_id
    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
    // get the image URL
    $image = wp_get_attachment_url( $thumbnail_id ); 
    // print the IMG HTML
	//echo '<img src="'.$image.'" alt="" width="762" height="365" />';
	$term_meta_test = get_woocommerce_term_meta($cat->term_id,'_ehshop_term_test_field', true);
	$term_meta_test_data = get_term_meta($cat->term_id, 'wh_meta_desc', true);
	?>
<style>
.hero-cat {
 background: #e08d72;
    display: flex;
    justify-content: center;
    align-items: center;
    font-family: 'Raleway';
    text-transform: uppercase;
    color: white;
    font-size: 3em;
    font-weight: 300;
	 height:400px; 
	 }
	 .hero-cat h1 {
		 margin:0;
	 }
<?php
if($term_meta_test == 'on'){ ?>
.hero-cat {
	 background: url("<?php echo $image;?>")  center no-repeat / cover;
	 height:400px; 
	 }

<?php } else { ?>
	.hero-cat { }
	
<?php } ?>
.hero-cat::after {
	content:  "<?php print_r($cat->description); ?>";
}
</style>
<div class="hero-cat">
<h1><?php single_cat_title(); ?></h1>
<?php if('' !== $cat->description && $term_meta_test !=='on' ) { ?>
<p><?php print_r($cat->description); ?></p>
<?php } ?>
<?php } ?>
<?php if (is_shop()){ echo "is shop"; } else { echo "Mooother!"; }?>
</div>
<ul class="products">
	<?php
	/* 	$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile;
		} else {
			echo __( 'No products found' );
		}
		wp_reset_postdata(); */
	?>
</ul><!--/.products-->
<?php //get_sidebar('sidebar-1'); ?>
<?php echo do_shortcode( '[woof]' ); ?>
<?php
/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
//do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );