<?php
/**
 * Customizer sections.
 *
 * @package eh-shop
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ehshop_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ehshop_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'ehshop' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ehshop_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'ehshop' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'ehshop' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ehshop_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'ehshop' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ehshop_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'ehshop' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ehshop_customize_sections' );
