<?php
add_action( 'cmb2_init', 'cmb2_add_metabox_best_sellers' );
function cmb2_add_metabox_best_sellers() {

	$prefix = '_ehshop_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'best_sellers_container',
		'title'        => __( 'Best Sellers', 'ehshop' ),
		'object_types' => array( 'page', 'post' ),
		'context'      => 'normal',
		'priority'     => 'default',
		'show_names'     => true,
	) );

	$best_seller_group_title = $cmb->add_field( array(
		'name' => __( 'Section title imag', 'ehshop' ),
		'title'        => __( 'Product grid item', 'ehshop' ),
		'id' => $prefix . 'best_seller_group_title',
		'type' => 'text',
    ) );

	$best_seller_group = $cmb->add_field( array(
		'title'        => __( 'Best seller group', 'ehshop' ),
		'id' => $prefix . 'best_seller_group',
		'type' => 'group',
		//'show_names'     => false,
		'options' => array(
			'group_title' => __( 'Item {#}', 'ehshop' ),
			'add_button' => __( 'add another editor', 'ehshop' ),
        ),
    ) );
    
       $img_bs = $cmb->add_group_field( $best_seller_group, array(
		//'name' => __( 'Best seller image', 'ehshop' ),
		'id' => $prefix . 'image_bs',
        'type' => 'file',
        'options' => array(
		'url' => false, // Hide the text input for the url
	),
    ) );
    
    $item_brand_bs =  $cmb->add_group_field( $best_seller_group, array(
		'name' => __( 'Brand', 'ehshop' ),
		'id' => $prefix . 'item_brand_bs',
		'type' => 'text_medium',
    ) );
    
    $item_title_bs =  $cmb->add_group_field( $best_seller_group, array(
		'name' => __( 'Product', 'ehshop' ),
		'id' => $prefix . 'item_title_bs',
		'type' => 'text_medium',
    ) );

    $item_price_bs =  $cmb->add_group_field( $best_seller_group, array(
		'name' => __( 'Price', 'ehshop' ),
		'id' => $prefix . 'item_price_bs',
		'type' => 'text_medium',
    ) );
    
$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmb);
$cmb2GroupGrid = $cmb2Grid->addCmb2GroupGrid($best_seller_group);
$row = $cmb2GroupGrid->addRow();
/*
$row->addColumns(array(
    array($product_image_group, 'class' => 'col-md-12')
));*/
}