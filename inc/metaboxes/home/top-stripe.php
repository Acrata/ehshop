<?php
/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action( 'cmb2_admin_init', 'ehshop_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function ehshop_register_theme_options_metabox() {

	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => '_ehshop_option_metabox',
		'title'        => esc_html__( 'EcoHealthy Shop Options', 'myprefix' ),
		'object_types' => array( 'options-page' ),

		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */

		'option_key'      => 'ehshop_theme_options', // The option key and admin menu page slug.
		 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
	) );

	/*
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */

	$cmb_options->add_field( array(
		'name' => __( 'Bara home', 'myprefix' ),
		'desc' => __( 'Texto a manera de banner', 'myprefix' ),
		'id'   => 'top_stripe_text',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_options->add_field( array(
		'name'    => __( 'Test Color Picker', 'myprefix' ),
		'desc'    => __( 'field description (optional)', 'myprefix' ),
		'id'      => 'test_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#bada55',
	) );

	$group_field_id = $cmb_options->add_field( array(
	'id'          => 'wiki_test_repeat_group',
	'type'        => 'group',
	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    'repeatable'  => false, // use false if you want non-repeatable group
	'options'     => array(
		'group_title'   => __( 'Header Options', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
		'add_button'    => __( 'Add Another Entry', 'cmb2' ),
		'remove_button' => __( 'Remove Entry', 'cmb2' ),
		'sortable'      => true, // beta
		// 'closed'     => true, // true to have the groups closed by default
	),
) );

// Id's for group's fields only need to be unique for the group. Prefix is not needed.
$cmb_options->add_group_field( $group_field_id, array(
	'name' => 'Entry Title',
	'id'   => 'title',
	'type' => 'text',
	 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );

$cmb_options->add_group_field( $group_field_id, array(
	'name' => 'Description',
	'description' => 'Write a short description for this entry',
	'id'   => 'description',
	'type' => 'textarea_small',
) );

$cmb_options->add_group_field( $group_field_id, array(
	'name' => 'Entry Image',
	'id'   => 'image',
	'type' => 'file',
	 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );

$cmb_options->add_group_field( $group_field_id, array(
	'name' => 'Image Caption',
	'id'   => 'image_caption',
	'type' => 'text',
) );

}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function myprefix_get_option( $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( '_ehshop_options', $key, $default );
	}

	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( '_ehshop_options', $default );

	$val = $default;

	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}

	return $val;
}


