<?php
add_action( 'cmb2_init', 'cmb2_add_metabox_cinfo' );
function cmb2_add_metabox_cinfo() {

	$prefix = '_ehshop_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'cinfo_container',
		'title'        => __( 'Contact info', 'ehshop' ),
		'object_types' => array( 'page', 'post' ),
		'show_names'     => false,
		'context'      => 'normal',
		'priority'     => 'default',
	) );


    $services_section_title = $cmb->add_field( array(
	'name'    => 'Section title',
	'desc'    => 'Titulo de la sección de servicios',
	'default' => 'Diseño ....',
	'id'      =>  $prefix . 'cinfo_section_title',
	'type'    => 'text_medium'
) );

	$cinfo_group = $cmb->add_field( array(
		'title'        => __( 'Services group', 'ehshop' ),
		'id' => $prefix . 'cinfo_group',
		'type' => 'group',
		//'show_names'     => true,
		'options' => array(
			'group_title' => __( 'Service {#}', 'ehshop' ),
			'add_button' => __( 'add another service', 'ehshop' ),
        ),
    ) );
    
       $img_bs = $cmb->add_group_field( $cinfo_group, array(
		'name' => __( 'Service image', 'ehshop' ),
		'id' => $prefix . 'image_service',
        'type' => 'file',
        'options' => array(
		'url' => false, // Hide the text input for the url
	),
    ) );
    
  	$service_color_bg = $cmb->add_group_field( $cinfo_group, array(
	'name'    => 'Color Picker',
	'id'      => $prefix . 'service_bg_colorpicker',
	'type'    => 'colorpicker',
	'default' => '#ffffff',
	// 'options' => array(
	// 	'alpha' => true, // Make this a rgba color picker.
	// ),
) ); 
    
   $service_title = $cmb->add_group_field( $cinfo_group, array(
	'name'    => 'Service title',
	'desc'    => 'field description (optional)',
	'id'      => $prefix . 'service_title',
	'type'    => 'wysiwyg',
	'options' => array(
	    'wpautop' => true, // use wpautop?
	    'media_buttons' => true, // show insert/upload button(s)
	    'textarea_rows' => get_option('default_post_edit_rows', 3), // rows="..."
	    'tabindex' => '',
	    'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
	    'editor_class' => '', // add extra class(es) to the editor textarea
	    'teeny' => false, // output the minimal editor config used in Press This
	    'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
	    'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
	    'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
	),
) );

    $service_link = $cmb->add_group_field($cinfo_group,  array(
	    'name' => __( 'Link URL', 'cmb2' ),
	    'id'   => $prefix . 'service_url',
	    'type' => 'text_url',
	// 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
) );
    
$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmb);
$cmb2GroupGrid = $cmb2Grid->addCmb2GroupGrid($cinfo_group);
$row = $cmb2GroupGrid->addRow();
/*
$row->addColumns(array(
    array($product_image_group, 'class' => 'col-md-12')
));*/
}