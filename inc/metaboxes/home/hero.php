<?php 

add_action( 'cmb2_init', 'cmb2_add_metabox' );
function cmb2_add_metabox() {

	$prefix = '_ehshop_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'hero_container',
		'title'        => __( 'Hero image', 'ehshop' ),
		'object_types' => array( 'page', 'post' ),
		'context'      => 'normal',
        'priority'     => 'default',
        'show_on'      => array( 'key' => 'page-template', 'value' => 'template-home.php' ),
        'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );

	$cmb->add_field( array(
		'name' => 'Hero image',
		'id' => $prefix . 'hero_image',
		'type' => 'file',
        // Optional:
	'options' => array(
		'url' => false, // Hide the text input for the url
	),
	'text'    => array(
		'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
	),
	// query_args are passed to wp.media's library query.
	
	'preview_size' => 'thumbnail', // Image size to use when previewing in the admin.
    ) );

}

add_action( 'cmb2_init', 'cmb2_add_metabox_product_grid' );
function cmb2_add_metabox_product_grid() {

	$prefix = '_ehshop_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'product_grid_container',
		'title'        => __( 'Product grid', 'ehshop' ),
		'object_types' => array( 'page', 'post' ),
		'context'      => 'normal',
		'priority'     => 'default',
		'show_names'     => false,
	) );

	$product_image_group = $cmb->add_field( array(
		'title'        => __( 'Product grid item', 'ehshop' ),
		'id' => $prefix . 'product_image',
		'type' => 'group',
		'show_names'     => false,
		'options' => array(
			'group_title' => __( 'Item {#}', 'ehshop' ),
			'add_button' => __( 'add another editor', 'ehshop' ),
        ),
    ) );
    
       $img_pg = $cmb->add_group_field( $product_image_group, array(
		'name' => __( 'Product image', 'ehshop' ),
		'id' => $prefix . 'product_image_it',
		'type' => 'file',
    ) );
    
    $title_pg =  $cmb->add_group_field( $product_image_group, array(
		'name' => __( 'Title product', 'ehshop' ),
		'id' => $prefix . 'title_product',
		'type' => 'text_medium',
	) );
$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmb);
$cmb2GroupGrid = $cmb2Grid->addCmb2GroupGrid($product_image_group);
$row = $cmb2GroupGrid->addRow();
/*
$row->addColumns(array(
    array($product_image_group, 'class' => 'col-md-12')
));*/
}

		add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
  #_ehshop_product_image_repeat ,
   #_ehshop_best_seller_group_repeat,
   #_ehshop_services_group_repeat {
	  display: flex;
    flex-flow: row wrap;
  }
  .cmb2-wrap .cmb-row {
      flex: 33%;
    padding: 0px 23px !important;
  }
  .cmb2-media-status .img-status {
      width:100% !important;
  }
  </style>';
}