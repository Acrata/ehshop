
<?php 

require get_template_directory() . '/inc/metaboxes/home/hero.php';
require get_template_directory() . '/inc/metaboxes/home/best-sellers.php';
require get_template_directory() . '/inc/metaboxes/home/services.php';
require get_template_directory() . '/inc/metaboxes/home/contact-info.php';
require get_template_directory() . '/inc/metaboxes/home/top-stripe.php';
require get_template_directory() . '/inc/metaboxes/home/term-test.php';
require get_template_directory() . '/inc/metaboxes/home/term-test-cond.php';
