<?php 

add_action( 'cmb2_admin_init', 'ehshop_register_taxonomy_metaboxa' );
/**
 * Hook in and add a metabox to add fields to taxonomy terms
 */
function ehshop_register_taxonomy_metaboxa() {
	$prefix = 'ehshop_term_';
	/**
	 * Metabox to add fields to categories and tags
	 */
	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => esc_html__( 'Category Metabox', 'cmb2' ), // Doesn't output for term boxes
		'object_types'     => array( 'term' ), // Tells CMB2 to use term_meta vs post_meta
		'taxonomies'       => array( 'category', 'pa_socavando' ), // Tells CMB2 which taxonomies should have these fields
		// 'new_term_section' => true, // Will display in the "Add New Category" section
	) );
	$cmb_term->add_field( array(
		'name'     => esc_html__( 'Extra Info', 'cmb2' ),
		'desc'     => esc_html__( 'field description (optional)', 'cmb2' ),
		'id'       => $prefix . 'extra_info',
		'type'     => 'title',
		'on_front' => false,
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Term Image', 'cmb2' ),
		'desc' => esc_html__( 'field description (optional)', 'cmb2' ),
		'id'   => $prefix . 'avatar',
		'type' => 'file',
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Arbitrary Term Fieldaaa', 'cmb2' ),
		'desc' => esc_html__( 'field description (optional)', 'cmb2' ),
		'id'   => $prefix . 'term_text_field',
		'type' => 'text',
	'show_on_cb'   => 'tgm_exclude_from_new', // function should return a bool value
	) );
function tgm_exclude_from_new( $cmb_term ) {
	global $pagenow;
	$exclude_from = $cmb_term->prop( 'exclude_from', array( 'termsd.php' ) );
    $excluded = in_array( $pagenow, $exclude_from, true );
    global $wp_query;
var_dump($wp_query);
	return ! $excluded;
}
}


/**
 * Remove metabox from appearing on post new screens before the post has been saved.
 * Can also add additional screens to exclude via the `'exclude_from'` property.
 * @link https://github.com/WebDevStudios/CMB2/wiki/Adding-your-own-show_on-filters#example-exclude-on-new-post-screens wiki
 */
/* $cmb = new_cmb2_box( array(
	'id'           => 'exclude_for_ids',
	'title'        => 'Demo',
	'exclude_from' => array( 'post-new.php' ), // Exclude metabox on new-post screen
) ); */
/**
 * Removes metabox from appearing on post new screens before the post
 * ID has been set.
 * @author Thomas Griffin
 * @param  object $cmb CMB2 object
 * @return bool        True/false whether to show the metabox
 */